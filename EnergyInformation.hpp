#ifndef ENERGYINFORMATION_HPP
#define ENERGYINFORMATION_HPP
#include <string>

class EnergyInformation{

private:
    double cumulativeBrownEnergyConsumption = 0;
    double cumulativeEnergyConsumption = 0;
    double cumulativeGreenEnergyProduction = 0;
    double cumulativeRemainingGreenEnergy = 0;
    std::string dcName;

public:
    EnergyInformation() = default;
    EnergyInformation(double _cumulativeBrownEnergyConsumption,
                        double _cumulativeEnergyConsumption,
                        double _cumulativeGreenEnergyProduction,
                        double _cumulativeRemainingGreenEnergy,
                        std::string dc);
    double getCumulativeBrownEnergyConsumption();    
    double getCumulativeEnergyConsumption();    
    double getCumulativeGreenEnergyProduction();
    double getCumulativeRemainingGreenEnergy();
    std::string getDCName();
};

#endif