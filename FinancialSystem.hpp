#ifndef FINANCIALSYSTEM_HPP
#define FINANCIALSYSTEM_HPP


#include <string>
#include <iostream>
#include <fstream>
#include <simgrid/s4u.hpp>
#include "MailBoxSingleton.hpp"
#include "EnergyInformation.hpp"
#include "Message.hpp"
#include <math.h>  

class UtilisationCostSwitchActor;
class SellBuyPeriodActor;

class FinancialSystem
{


private:

double utilisation_cumulative = 0;
double utilisation_last_remaining = 0;
double utilisation_last_brown = 0;
double sell_cumulative = 0;
double buy_cumulative = 0;
double sell_buy_last_production = 0;
double sell_buy_last_consumption = 0;
double buying_cost = 0;
double selling_gain = 0;
double utilisation_cost = 0;
double nextOutputTime = 0;

std::string outputToPrint;
void logResults(double time);
simgrid::s4u::ActorPtr utilisation_switch_process;
simgrid::s4u::ActorPtr sell_buy_period_process;
/**
 * The output stream of the system.
 */
ofstream *output;    


public:

FinancialSystem();
FinancialSystem(double aBuying_cost, double aSelling_gain, double anAutoconso_cost, double anAutoconso_gain, simgrid::s4u::Host *aHost, double aFullUtilisationCost, double aLightUtilisationCost, double aFullCostPeriod, double aLightCostPeriod, double autoconsoPeriod, double sellBuyPeriod);

void updateUtilisation(double time);
void updateSellBuy(double time);
void setUtilisationCost(double newUtilisationCost);
void start();
void stop();




};
#endif