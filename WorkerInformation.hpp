
#ifndef WORKERINFORMATION_HPP
#define WORKERINFORMATION_HPP

#include <string>
#include <unordered_map>
#include "robin_hood.h"
using namespace std; 

class DeployableVM;
class WorkerInformation {


private:
    int availableCores;
    double availableRAM;
    std::string hostName;
    bool isOn;
    std::string dcName;
    DeployableVM *vm = nullptr;
    robin_hood::unordered_map<std::string, DeployableVM*> *runningVMS = nullptr;

public:
    WorkerInformation(int cores, double RAM, std::string hostName, std::string dc, bool on, robin_hood::unordered_map<std::string, DeployableVM*> *vms);
    int getAvailableCores() ;
    void setAvaliableCores(int cores);
    void setAvaiableRAM(double RAM);
    double getAvailableRAM() ;
    bool serverIsOn() ;
    std::string *getHostName() ;
    std::string *getDCName() ;
    robin_hood::unordered_map<std::string, DeployableVM*> *getRunningVMS();
    void setIsOn(bool on);
};

#endif