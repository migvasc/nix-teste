#ifndef MIGRATIONPLAN_HPP
#define MIGRATIONPLAN_HPP

#include <simgrid/s4u.hpp>
#include "DeployableVM.hpp"

class MigrationPlan{

private:
	/**
	 * The server where the VM would be migrated.
	 */
    simgrid::s4u::Host *destination= nullptr;
    	
	/**
	 * The VM to migrate.
	 */
    DeployableVM *vm= nullptr;

	/**
	 * The time the migration is planned.
	 */
	double time;
    /**
	 * The migration time calculated considering the best scenario for latency
	 */
	double bestMigrationTime;
	/**
	 * The migration time calculated considering the worst scenario for latency
	 */
	double worstMigrationTime;

public:
    MigrationPlan(double aTime, simgrid::s4u::Host *aHost,DeployableVM *aVM, double bestTime, double worstTime);    
    simgrid::s4u::Host *getHost();
    DeployableVM *getVM();
    double getTime();
    double getBestMigrationTime();
    double getWorstMigrationTime();
    void setHost(simgrid::s4u::Host *host);
    void setVM(DeployableVM * vm);
    void setTime(double time);
    void setBestMigrationTime(double bestTime);
    void getWorstMigrationTime(double wortTime);

};
#endif
