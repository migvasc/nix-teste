#include "FinancialSystem.hpp"
#include "UtilisationCostSwitchActor.hpp"
#include "SellBuyPeriodActor.hpp"

FinancialSystem::FinancialSystem(double aBuying_cost, double aSelling_gain, double anAutoconso_cost, double anAutoconso_gain, simgrid::s4u::Host *aHost, double aFullUtilisationCost, double aLightUtilisationCost, double aFullCostPeriod, double aLightCostPeriod, double autoconsoPeriod, double sellBuyPeriod)
{
    utilisation_cumulative = 0;
    utilisation_last_brown = 0;
    utilisation_last_remaining = 0;
    sell_cumulative = 0;
    buy_cumulative = 0;
    sell_buy_last_consumption = 0;
    sell_buy_last_production = 0;	
    selling_gain = aSelling_gain;
    buying_cost = aBuying_cost;
    utilisation_cost = aFullUtilisationCost;		
    nextOutputTime = 0.0;
    std::string separator = ";";
    outputToPrint = "0.0"+separator+"0.0"+separator+std::to_string(utilisation_cumulative)+separator+separator+std::to_string(buy_cumulative)+separator+std::to_string(sell_cumulative)+"\n";

    try 
    { 
        output = new ofstream();
        output->open("output/financial_costs.csv");            
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;
    }

    utilisation_switch_process = simgrid::s4u::Actor::create("utilisation_cost_switch", aHost,
                                    UtilisationCostSwitchActor(aFullUtilisationCost, aLightUtilisationCost, aFullCostPeriod, aLightCostPeriod, this));   
                        
    sell_buy_period_process  = simgrid::s4u::Actor::create("sell_buy_period", aHost, SellBuyPeriodActor(sellBuyPeriod, this));   

                                        
}

void FinancialSystem::logResults(double time)
{
    std::string separator = ";";
    
    if(time>nextOutputTime){		
        try 
        {
            *output << outputToPrint;
        }
        catch (const std::exception& e) 
        {            
            cout << "Error : " << e.what() << endl;
        }
        nextOutputTime = time;
    }
    
    double totalCost = utilisation_cumulative + buy_cumulative - sell_cumulative ;
    outputToPrint = std::to_string(time)+separator+std::to_string(totalCost)+separator+std::to_string(utilisation_cumulative)+separator+std::to_string(buy_cumulative)+separator+std::to_string(sell_cumulative)+"\n";

}
void FinancialSystem::updateUtilisation(double time)
{
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_GRID_GET_ENERGY_INFO));
    Message *msg;
    EnergyInformation* info;                 
    msg = MailBoxSingleton::GetInstance()->getMessage("grid_energy_info");
    if(msg!= nullptr)
    {
        info = static_cast<EnergyInformation*>(msg->data);
    }

    double new_utilisation_brown = info->getCumulativeBrownEnergyConsumption();
    double new_utilisation_remaining = info->getCumulativeRemainingGreenEnergy();
    // conversion in kWh
    new_utilisation_brown /= (3600.0 * 1000.0);
    new_utilisation_remaining /= (3600.0 * 1000.0);

    double utilisation_timestep_brown = new_utilisation_brown - utilisation_last_brown;
    double utilisation_timestep_remaining = new_utilisation_remaining - utilisation_last_remaining;
    utilisation_cumulative += utilisation_cost * min(utilisation_timestep_remaining, utilisation_timestep_brown);
    utilisation_last_brown = new_utilisation_brown;
    utilisation_last_remaining = new_utilisation_remaining;
    
    logResults(time);


}
void FinancialSystem::updateSellBuy(double time)
{
    //
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_GRID_GET_ENERGY_INFO));
    simgrid::s4u::this_actor::yield();
    Message *msg;
    EnergyInformation* info;                 
    msg = MailBoxSingleton::GetInstance()->getMessage("grid_energy_info");
    if(msg!= nullptr)
    {
        info = static_cast<EnergyInformation*>(msg->data);
    }
    double new_prod_cumulative = info->getCumulativeGreenEnergyProduction();
    // conversion in kWh
    new_prod_cumulative /= (3600.0 * 1000.0);
    
    double prod_timestep = new_prod_cumulative - sell_buy_last_production;
    
    double new_conso_cumulative = info->getCumulativeEnergyConsumption();
    // conversion in kWh
    new_conso_cumulative /= (3600.0 * 1000.0);
    
    double conso_timestep = new_conso_cumulative - sell_buy_last_consumption;
    
    double diff_conso_prod = conso_timestep - prod_timestep;
    
    if(diff_conso_prod >= 0 )
    {
        buy_cumulative += diff_conso_prod * buying_cost;
    }
    else
    { 
        sell_cumulative += -selling_gain * diff_conso_prod;
    }
    
    sell_buy_last_production = new_prod_cumulative;
    sell_buy_last_consumption = new_conso_cumulative;

    logResults(time);
}
void FinancialSystem::setUtilisationCost(double newUtilisationCost)
{
    utilisation_cost = newUtilisationCost;
}

void FinancialSystem::start()
{
}

void FinancialSystem::stop()
{
    updateUtilisation(round(simgrid::s4u::Engine::get_clock()));
    utilisation_switch_process->kill();
    sell_buy_period_process->kill();   
}

