#include "VMExecutor.hpp"
XBT_LOG_NEW_DEFAULT_CATEGORY(vmexec, "vm executor category");

int VMExecutor::compute(double computeAmount, std::vector<simgrid::s4u::ExecPtr> *executions){                                                                        
    double begin = simgrid::s4u::Engine::get_clock();      
    simgrid::s4u::Host* host = simgrid::s4u::this_actor::get_host();
    if(dynamic_cast<simgrid::s4u::VirtualMachine *>(host)){
        host = (dynamic_cast<simgrid::s4u::VirtualMachine *>(host))->get_pm();
    }              

    // sg_host_energy_update_all();    
    //XBT_INFO("Flops to be computed %f",computeAmount);

    simgrid::s4u::ExecPtr ptr = simgrid::s4u::this_actor::exec_init(computeAmount);      
    executions->push_back(ptr);
    ptr->start();
    ptr->wait();
    //sg_host_energy_update_all();    
    double end = simgrid::s4u::Engine::get_clock();          
    //XBT_INFO("EXECUTED TIME: %f", end - begin);     
    return 0;
}

VMExecutor::VMExecutor() = default;

VMExecutor::VMExecutor(DeployableVM* objVM, double flops, std::vector<simgrid::s4u::ExecPtr> *executionsVector)
{
    deployableVM = objVM;
    flopAmount = flops;
    executions = executionsVector;
}

void VMExecutor::operator()()
{           
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));         
    createExecutorsActors();
    waitAllTasksToFinish();       
    onVMExecutionFinished();         
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    
}


void VMExecutor::createExecutorsActors(){
    for(int i=0; i<deployableVM->getVM()->get_core_count();i++)
    {
        actors.push_back(simgrid::s4u::Actor::create("executor"+std::to_string(i)+deployableVM->getName(), deployableVM->getVM(), compute,flopAmount,executions)); 
    }    
}

void VMExecutor::waitAllTasksToFinish(){

  //wait for all ExecPtr to be inserted on the vector
  simgrid::s4u::this_actor::sleep_for(1);
  
  
  //wait to finish all the executions
  while (not (*executions).empty()) {

    int changed_pos = simgrid::s4u::Exec::wait_any_for(executions,-1);
    simgrid::s4u::ExecPtr ptr =  *((*executions).begin() + changed_pos);
    if(ptr->get_remaining() == 0 )
    {
        (*executions).erase((*executions).begin() + changed_pos);
    } 

  }


  //XBT_INFO("ENERGY CONSUMED BY HOST %s : %f",deployableVM->getVM()->get_pm()->get_cname(),sg_host_get_consumed_energy(deployableVM->getVM()->get_pm()));
 
}

void VMExecutor::onVMExecutionFinished(){            
    //XBT_INFO("ON EXECUTION FINISHED");
    
    sg_host_energy_update_all();
    simgrid::s4u::VirtualMachine *vm = deployableVM->getVM();
    std::string vmName =vm ->get_name();
    simgrid::s4u::Host * h = vm->get_pm();
    
    deployableVM->getWorkerActor()->handleExecutionFinished(deployableVM->getVM()->get_name());
    
    

    
    for(simgrid::s4u::ActorPtr taskExecutor: actors){
        taskExecutor->kill();
    }
    //cout.precision(10);
    //cout <<   fixed <<  simgrid::s4u::Engine::get_clock() << " exec finished vm " << vmName << "  " << h->get_name() << " " << fixed << sg_host_get_consumed_energy(h) << endl;     
    
    simgrid::s4u::this_actor::exit();
}
