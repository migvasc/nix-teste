#include "VMResourceUsageActor.hpp"

VMResourceUsageActor::VMResourceUsageActor(DeployableVM *aVM, std::vector<std::pair<int,int>> history )
{
    vm = aVM;
    resource_usage_history = history;
    original_ram_size = vm->getVM()->get_ramsize();
    original_cpu_bound = simgrid::s4u::this_actor::get_host()->get_speed() * vm->getVM()->get_core_count();
}

void VMResourceUsageActor::operator()()
{
    for(std::pair<int,int> item : resource_usage_history)
    {
        simgrid::s4u::this_actor::sleep_for(300);        
        vm->getVM()->set_bound( original_cpu_bound * (item.first/100.0));                
        vm->getVM()->set_ramsize( original_ram_size* (item.second/100.0));
        vm->setcurrentCPUUsage((item.first/100.0));
        vm->setCurrentRAMUsage((item.second/100.0));
    }
    
}