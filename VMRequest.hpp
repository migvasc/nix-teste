#ifndef VMREQUEST_HPP
#define VMREQUEST_HPP

#include "VMInterface.hpp"
#include <simgrid/s4u.hpp>

class VMRequest : public VMInterface {

private:
    double ramsize;
    double flopAmount;
    std::string id;
    simgrid::s4u::VirtualMachine* vm = nullptr;
    std::string destHost;
    std::vector<std::pair<int,int>> resourceUsageHistory;
    double duration;
public:
    /** 
     *
     * @tableofcontents
     *
     * A VMRequest represents a virtual machine (or a container) that hosts actors.
     * The total computing power that the contained actors can get is constrained to the virtual machine size.
     *
     */
    VMRequest(int cores, double ram, double duration, std::string vmID);
    VMRequest(int cores, double ram, double duration, std::string vmID, const std::vector<std::pair<int,int>>&);
    double getRamSize();
    int getNbCores();
    int ncores;
    double getFlopAmount();
    simgrid::s4u::VirtualMachine* getVM();
    void setVM(simgrid::s4u::VirtualMachine* vm_);
    void setDestHost(std::string host);
    std::string getDestHost();
    std::string getName();    
    double getRemainingDuration();
    std::vector<std::pair<int,int>> getVMResourceUsageHistory(); 
};

#endif