#ifndef DEPLOYABLEVM_HPP
#define DEPLOYABLEVM_HPP
#include "simgrid/s4u.hpp"
#include "VMExecutor.hpp"
#include "VMInterface.hpp"
#include "VMResourceUsageActor.hpp"
#include <math.h>  

class WorkerActor;

class DeployableVM : public VMInterface
{

private: 
    simgrid::s4u::VirtualMachine* vm= nullptr;
    double startTime;
    double computeAmount;    
    double currentCPUUsage = 100.0;
    double currentRAMUsage = 100.0;
    bool isMigrating;    
    std::vector<simgrid::s4u::ExecPtr> executions;
    WorkerActor *workerActor= nullptr;

public:
    DeployableVM(std::string id, simgrid::s4u::Host* host, int ncores, double ram, double flops, WorkerActor *actor);
    simgrid::s4u::VirtualMachine* getVM();
    double getRuntime();
    bool isVMMigrating();
    void setIsVMMigrating(bool migrating);
    double getRemainingDuration();
    void startVM();
    void startsExecuting();
    void setWorkerActor(WorkerActor *actor);
    WorkerActor *getWorkerActor();
    std::string getName() ;
    int getNbCores();
    double getRamSize();
    double getcurrentCPUUsage();
    void setcurrentCPUUsage(double usage);
    double getCurrentRAMUsage();
    void setCurrentRAMUsage(double usage);

};

#endif