#include "PVActor.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(pv, "pv category");

 PVActor::PVActor() = default;
 PVActor::PVActor(std::string input){		
   // //XBT_INFO("PV ACTOR created");
    
    inputFile = input;
   // //XBT_INFO("NAME OF INPUT FILE");
    //XBT_INFO(inputFile.c_str());
    
    time = 0;    
                
    currentGreenPowerProduction = 0;
} 

void PVActor::operator()() {     
    string logInfo = "I start monitoring the green power production of DC " +getName();
  //  //XBT_INFO(logInfo.c_str());
    ifstream input;
    try {
    
        input =  ifstream(inputFile);
        advanceTime(&input);  
        Message* message = new Message(MESSAGE_UPDATE_ENERGY);
        time = 0;
                
        currentGreenPowerProduction = nextGreenPowerProduction;
        
        while(nextTime != numeric_limits<double>::max() ){                
            double sleep_amount = nextTime - time;      
            //XBT_INFO("ANTES DO SLEEP FOR");
            simgrid::s4u::this_actor::sleep_for(sleep_amount);
            //XBT_INFO("DPS DO SLEEP FOR");	          
            MailBoxSingleton::GetInstance()->sendMessage(std::string("grid"),message );             
            //XBT_INFO("ANTES DO ADVANCE TIME");	          
            advanceTime(&input);
            //XBT_INFO("DPS DO ADVANCE TIME");	          
            logInfo = std::to_string(simgrid::s4u::Engine::get_clock()) +  "  |new green power production for DC "+getName()+" : "+ to_string(currentGreenPowerProduction)+"W";
            //XBT_INFO(logInfo.c_str());                
        }    

        input.close();            

    } catch (const std::exception& e) 
    {            
        cout << "Error : at pvactor operator  " << simgrid::s4u::this_actor::get_name() << " " << e.what() << endl;

    }	
    
}


/**
 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
 */
void PVActor::advanceTime(ifstream *input) {
     simgrid::s4u::CommPtr ptr = nullptr;
    try {
        string  nextLine;
        
        
        if(getline(*input,nextLine))
        {            
         //   //XBT_INFO("OLD currentGreenPowerProduction");
           // //XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            currentGreenPowerProduction = nextGreenPowerProduction;
          //  //XBT_INFO("UPDATE currentGreenPowerProduction");
         //   //XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            std::vector<std::string> nextInput;				
            // TODO: GET FROM THIS FILE THE SEPARATOR
             //std::string nextInput ;//= nextLine.split(MasterVMManager.FILE_SEPARATOR);
            boost::algorithm::split(nextInput, nextLine, boost::is_any_of(";"));
            nextTime = atof(nextInput[0].c_str());
            nextGreenPowerProduction = atof(nextInput[1].c_str());
            time = simgrid::s4u::Engine::get_clock();				            
        }
        else 
        {
            nextTime = numeric_limits<double>::max();
        }

    } catch (const std::exception& e) 
    {            
        cout << "Error at pvactor advancetime " << simgrid::s4u::this_actor::get_name()  << " " << e.what() << endl;

    }		
      
}


double PVActor::getCurrentGreenPowerProduction(){
  //  //XBT_INFO("CURRENT GREEN POW %f",currentGreenPowerProduction);
    return currentGreenPowerProduction;
}



string PVActor::getName(){
    return simgrid::s4u::this_actor::get_cname();
}

