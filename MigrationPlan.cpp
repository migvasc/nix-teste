#include "MigrationPlan.hpp"




MigrationPlan::MigrationPlan(double aTime, simgrid::s4u::Host *aHost,DeployableVM *aVM, double bestTime, double worstTime)
{
    time = aTime;
    destination = aHost;
    vm = aVM;
    bestMigrationTime = bestTime;
    worstMigrationTime = worstTime;
}

simgrid::s4u::Host *MigrationPlan::getHost()
{
    return destination;
}


DeployableVM *MigrationPlan::getVM()
{
    return vm;
}

double MigrationPlan::getTime()
{
    return time;
}

double MigrationPlan::getBestMigrationTime()
{
    return bestMigrationTime;
}

double MigrationPlan::getWorstMigrationTime()
{
    return worstMigrationTime;
}

void MigrationPlan::setHost(simgrid::s4u::Host *host)
{
    destination = host;
}

void MigrationPlan::setVM(DeployableVM * aVM)
{
    vm = aVM;
}

void MigrationPlan::setTime(double aTime)
{
    time = aTime;
}
void MigrationPlan::setBestMigrationTime(double bestTime)
{
    bestMigrationTime = bestTime;
}

void MigrationPlan::getWorstMigrationTime(double worstTime)
{
    worstMigrationTime = worstTime;
}