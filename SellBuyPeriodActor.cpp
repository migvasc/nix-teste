#include "SellBuyPeriodActor.hpp"



SellBuyPeriodActor::SellBuyPeriodActor(double anUpdateTime, FinancialSystem *aFinancialSystem)
{
    updateTime = anUpdateTime;    
    financial = aFinancialSystem;
}

void SellBuyPeriodActor::operator()()
{
    registerObject("sellBuyPeriodActor",this);
    while(true)
    {
        simgrid::s4u::this_actor::sleep_for(updateTime);                    
        MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));
        financial->updateSellBuy(round(simgrid::s4u::Engine::get_clock()));
        financial->updateUtilisation(round(simgrid::s4u::Engine::get_clock()));
    } 
}

void SellBuyPeriodActor::handleMessage(Message *message)
{

}
        