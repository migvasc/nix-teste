#ifndef VMDISPATCHER_HPP
#define VMDISPATCHER_HPP


#include <simgrid/s4u.hpp>
#include "xbt/replay.hpp"
#include "xbt/str.h"
#include "VMRequest.hpp"
#include "Message.hpp"
#include <cstdlib>
#include "MailBoxSingleton.hpp"
#include<bits/stdc++.h> 
class VMDispatcher {

public:    
    explicit VMDispatcher(std::vector<std::string> args);
    void operator()();
    static void sendVM(simgrid::xbt::ReplayAction& action);
    static void sendVMHist(simgrid::xbt::ReplayAction& action);
    static void stop(simgrid::xbt::ReplayAction& action);
    static void waitFor(double time);

};

#endif