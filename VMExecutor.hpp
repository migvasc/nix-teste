#ifndef VMEXECUTOR_HPP
#define VMEXECUTOR_HPP
#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "WorkerActor.hpp"
#include "DeployableVM.hpp"
#include "Message.hpp"
#include "MailBoxSingleton.hpp"
#include <math.h>  

class VMExecutor{

private:
    static int compute(double computeAmount, std::vector<simgrid::s4u::ExecPtr> *executions) ;    
    std::vector<simgrid::s4u::ActorPtr> actors;        
    double flopAmount;
    std::vector<simgrid::s4u::ExecPtr> *executions= nullptr;
    DeployableVM *deployableVM= nullptr;
    int finishedTasks;
    void createExecutorsActors();
    void waitAllTasksToFinish();
    void onVMExecutionFinished();

public:
    VMExecutor();
    VMExecutor(DeployableVM* objVM, double flops, std::vector<simgrid::s4u::ExecPtr> *executionsVector);
    void operator()();

};

#endif