#ifndef VMINTERFACE_HPP
#define VMINTERFACE_HPP
#include <string>
class VMInterface{

public:

/**
 * Return the VM name.
 * @return The VM name.
 */
virtual std::string getName() = 0;
    
/**
 * Return the number of cores used by the VM.
 * @return The number of cores used by the VM.
 */
virtual int getNbCores() = 0;


/**
 * Return the remaining duration of execution of the VM.
 * @return The remaining duration of execution of the VM.
 */
virtual double getRemainingDuration() =0;
};
#endif