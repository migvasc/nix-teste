#ifndef WORKERACTOR_HPP
#define WORKERACTOR_HPP


#include <simgrid/s4u.hpp>
#include "Message.hpp"
#include "VMRequest.hpp"
#include "WorkerInformation.hpp"
#include "MailBoxSingleton.hpp"
#include "CommunicationInterface.hpp"

#include <math.h>  

class DeployableVM;
class WorkerActor : CommunicationInterface {

  std::string dcName;
  std::string serverName;
  robin_hood::unordered_map<std::string, DeployableVM*> runningVMS;
  int getAvailableCores(std::string serverName);  
  void startExecutingVM(VMRequest* vm);
  void startMigratingVM(void * data);    
  void handleVMMigrationFinished(DeployableVM *vm);
  void handleVMMigrationStarted(void *data);
public:
  explicit WorkerActor(std::string serverName, std::string dc);  
  explicit WorkerActor() = default;  
  void operator()();
  void handleExecutionFinished(std::string vmID);
  void removeRunningVMFromMemory(std::string vmID);
  void sendUpdatedWorkerInformation(std::string hostName);
  void handleMessage(Message *message);    
  std::string getDCName();
  std::string getServerName();
  robin_hood::unordered_map<std::string, DeployableVM*> getRunningVMS();
  
};

#endif