import time
import concurrent.futures


start = time.perf_counter()

def do_something(seconds):
    print(f'sleep {seconds} s')
    time.sleep(seconds)
    return 'terminei de dormir'

with concurrent.futures.ProcessPoolExecutor() as executor:
    results = [executor.submit(do_something,1) for _ range(10)] 

finish = time.perf_counter()
print(f'finished in {round(finish-start,2)} second(s)')



#ps = []
#
#for _ in range(10):
#    p = multiprocessing.Process(target = do_something,args=[1.5])
#    p.start()
#    ps.append(p)
#
#for p in ps:
#    p.join()
#
#

#