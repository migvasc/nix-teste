#include "PVPanel.hpp"

 PVPanel::PVPanel() = default;
 PVPanel::PVPanel(std::string fileName){		
   
    inputFile = fileName;        
    time = 0;                  
    nextTime=-1;  
    loadCSVFile();
    currentGreenPowerProduction = 0;
    advanceTime(0);

} 


void PVPanel::loadCSVFile(){
    ifstream input = ifstream(inputFile);
    try {
        string  nextLine;
        while(getline(input,nextLine))
        {                                    
            std::vector<std::string> nextInput;				
            boost::algorithm::split(nextInput, nextLine, boost::is_any_of(";"));
            greenPowerProduction.push_back(make_pair(atof(nextInput[0].c_str()),atof(nextInput[1].c_str())));
        }
        input.close();
    } catch (const std::exception& e) 
    {            
        cout << "Error at PVPanel advancetime " << simgrid::s4u::this_actor::get_name()  << " " << e.what() << endl;
    }		
}

/**
 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
 */
void PVPanel::advanceTime(double simulation_time) {
     simgrid::s4u::CommPtr ptr = nullptr;
    try {
        string  nextLine;
        while(nextTime<= simulation_time)
        {            
            if(greenPowerProduction.empty()) break;
            currentGreenPowerProduction = nextGreenPowerProduction;        
            std::pair<double,double>  data = greenPowerProduction[0];            
            time = nextTime;
            nextTime = data.first;
            nextGreenPowerProduction = data.second;            
            greenPowerProduction.erase(greenPowerProduction.begin());
        }
    } catch (const std::exception& e) 
    {            
        cout << "Error at PVPanel advancetime " << simgrid::s4u::this_actor::get_name()  << " " << e.what() << endl;
    }		
      
}


double PVPanel::getCurrentGreenPowerProduction(double simulation_time){
    advanceTime(simulation_time);
    return currentGreenPowerProduction;
}
