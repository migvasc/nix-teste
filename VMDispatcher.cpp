
#include "VMDispatcher.hpp"
#include "simgrid/plugins/energy.h"


XBT_LOG_NEW_DEFAULT_CATEGORY(vmDispatcher, "vmDispatcher category");

VMDispatcher::VMDispatcher(std::vector<std::string> args)
{
    //XBT_INFO("criou o vmdispatcherrr");
    const char* actor_name     = args.at(0).c_str();
    const char* trace_filename = args.size() > 1 ? args[1].c_str() : nullptr;
    //XBT_INFO("ARGS SIZE %d", args.size());
    //XBT_INFO("actor_name");
    //XBT_INFO(actor_name);
    //XBT_INFO("trace_filename");    
    //XBT_INFO(trace_filename);
    simgrid::xbt::replay_runner(actor_name, trace_filename);
}

void VMDispatcher::operator()()
{
// Nothing to do here
}


void updateEnergy(){
    MailBoxSingleton::GetInstance()->sendMessage("grid",new Message(MESSAGE_UPDATE_ENERGY) );    
}

VMRequest* buildVMRequest(simgrid::xbt::ReplayAction& action){        
    std::string id = std::string(action[3]);
    int cores   =   std::stoi(action[4]); 
    double ram =    cores * 2 *1024*1024*1024;//std::stoi(action[5]) *1024*1024*1024;
    double duration =   std::stod(action[5]);        
    int cpuUsage = 0;  
    int ramUsage = 0;  
    std::vector<std::pair<int,int>> resources_usage_history;

    //Here we have data about the VM history
    int i = 6;
    /*while( i < action.size() -1 )
    {
        cpuUsage = std::stoi(action[++i]);  
        ramUsage = std::stoi(action[++i]);
        resources_usage_history.push_back(make_pair(cpuUsage,ramUsage));

    }
    
    
    */
    //XBT_INFO("RAM %f CORES %d flops %f id %s",ram,cores,duration,id.c_str());
    return new VMRequest(cores,ram,duration,std::string(id),resources_usage_history);
}

void sendVMMessage(VMRequest* vm){
    MailBoxSingleton::GetInstance()->sendMessage("manager",new Message(MESSAGE_VM_SUBMISSION,vm));
}

void VMDispatcher::sendVM(simgrid::xbt::ReplayAction& action){
    //XBT_INFO("RECEBI REQUEST PRA MANDAR");    
    double time  =  std::stod(action[2]);
    //XBT_INFO("JAJA EU VOu MANDAR");                
    waitFor(time);
    VMRequest* vmtobesent = buildVMRequest(action);
    //XBT_INFO("VM BUILT INFO, RAM %f CORES %d ID %s",vmtobesent->getRamSize(),vmtobesent->getNbCores(),vmtobesent->getName().c_str());
    sendVMMessage(vmtobesent);
    //XBT_INFO("EU MANDEI");
    //XBT_INFO("VO ATUALIZAR ENERGIA");

}

void VMDispatcher::sendVMHist(simgrid::xbt::ReplayAction& action){
    //XBT_INFO("RECEBI REQUEST PRA MANDAR");    
    double time  =  std::stod(action[2]);
    //XBT_INFO("JAJA EU VOu MANDAR");                
    waitFor(time);
    VMRequest* vmtobesent = buildVMRequest(action);
    //XBT_INFO("VM BUILT INFO, RAM %f CORES %d ID %s",vmtobesent->getRamSize(),vmtobesent->getNbCores(),vmtobesent->getName().c_str());
    sendVMMessage(vmtobesent);
    //XBT_INFO("EU MANDEI");
    //XBT_INFO("VO ATUALIZAR ENERGIA");

}


void VMDispatcher::stop(simgrid::xbt::ReplayAction& action){
    double time  =  std::stod(action[2]);
    waitFor(time);
    sg_host_energy_update_all();
    
    simgrid::s4u::Actor::kill_all();
    simgrid::s4u::this_actor::exit();
}

void VMDispatcher::waitFor(double time){
    double waitForTime = time - simgrid::s4u::Engine::get_clock();
    //XBT_INFO("CURRENT TIME %f",simgrid::s4u::Engine::get_clock());
    //XBT_INFO("WAIT FOR TIME: %f - %f = %f",time,simgrid::s4u::Engine::get_clock(),waitForTime);
    if(floor(waitForTime)>0.0){
        //XBT_INFO("I WILL WAIT FOR %f",waitForTime);
        simgrid::s4u::this_actor::sleep_for(waitForTime);
        //XBT_INFO("CURRENT TIME %f",simgrid::s4u::Engine::get_clock());
    }     
}

