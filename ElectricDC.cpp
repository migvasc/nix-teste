#include "ElectricDC.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(edc, "edc category");

/**
 * Creates the electric system of a single data-center.
 */
ElectricDC::ElectricDC(std::string aID, int nPV,std::string file) : ElectricSystem(("output/energy_"+aID+".csv")) 
{             
    id = aID;							
    nbPV = nPV;    
    inputFile = file;
    pvpanel = PVPanel(inputFile);    

}

void ElectricDC::init(){
    registerObject(id,this);        
    mutex = simgrid::s4u::Mutex::create();
    try {               
        power_output = new ofstream();
        power_output->open(("output/power_"+id+".csv"));
    } catch (const std::exception& e) 
    {            
        cout << "Error at ELECTRIC DC INIT: " << simgrid::s4u::this_actor::get_name() << " " << e.what()   << endl;
    }
            
    for(simgrid::s4u::Host *host : getHosts())
    {
        if(host == simgrid::s4u::this_actor::get_host()) continue;
     //   //XBT_INFO("HOST INFORMATION SENT %s",host->get_cname());
        WorkerInformation *wi = new WorkerInformation(host->get_core_count(),2000.00, host->get_name(),id, false,nullptr);        
        MailBoxSingleton::GetInstance()->sendMessage("manager",new Message(MESSAGE_HOST_INFORMATION,wi));
    }

}

void ElectricDC::operator()(){
    init();      
    while(true){
        simgrid::s4u::this_actor::sleep_for(100);
        sendGreenEnergyInfo();
    }
}

void ElectricDC::handleMessage(Message* message){        
    mutex->lock();
    switch (message->type)
    {
        case MESSAGE_UPDATE_ENERGY:
           // //XBT_INFO("MESSAGE_UPDATE_ENERGY");
            updateEnergy(round(simgrid::s4u::Engine::get_clock()));
            break;    
        case MESSAGE_GET_ENERGY:
            ////XBT_INFO("MESSAGE_GET_ENERGY");
            getEnergyInformation();
            break;
        case MESSAGE_TURN_HOST_ON:
            handleTurnHostOn(message->data);
            break;
        case MESSAGE_SHUTDOWN_HOST:
            handleShutdownHost(message->data);
            break;
        case MESSAGE_GET_GREEN_ENERGY:
            sendGreenEnergyInfo();
            break;
        default:
            break;
    }
    mutex->unlock();
}



void ElectricDC::handleTurnHostOn(void *data){
    VMRequest* vm = static_cast<VMRequest*>(data);
    simgrid::s4u::Host *h = simgrid::s4u::Host::by_name(vm->getDestHost());    
    turnHostOn(h,vm);        

}





/*
    Return all the hosts for the current data center, i.e. the hosts that are in the same netzone.
*/
vector<simgrid::s4u::Host*> ElectricDC::getHosts(){
    vector<simgrid::s4u::Host*> hosts =
     simgrid::s4u::Engine::get_instance()-> netzone_by_name_or_null(id)->get_all_hosts();        
    return hosts;
}




/**
 * Compute the current cumulative energy consumption of the data-centers according to SimGrid.
 * @return the current cumulative energy consumption of the data-centers.
 */
double ElectricDC::computeDcCurrentEnergyConsumption() {        
    double energy = 0;    
    double power  = 0;
    std::vector<simgrid::s4u::Host*> hosts = getHosts();        
    sg_host_energy_update_all();    
    if(not hosts.empty())
    {
        for(simgrid::s4u::Host* h : hosts )        
        {

            if(h == simgrid::s4u::this_actor::get_host()) continue;

            power = sg_host_get_consumed_energy(h);
            energy += power;
        }
    }
    return energy;
}


/**
 * Return the current local power production of the DC.
 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
 * @return The current local power production of the DC.
 */
double ElectricDC::getLocalPowerProduction(){    
    double result = pvpanel.getCurrentGreenPowerProduction(round(simgrid::s4u::Engine::get_clock()));
    return  result * nbPV;
}

/**
 * Return the power available for the local consumption AND for the output migrations.
 * It is equals to: local_production + input_migration.
 * @return The power available for the local consumption AND for the output migrations.
 */
double ElectricDC::getAvailablePower(){									
    return getLocalPowerProduction() ;
}


void ElectricDC::updateEnergy(double newTime) {	
    double greenPowerProduction = getLocalPowerProduction();
    double newCumulativeEnergyConsumption = computeDcCurrentEnergyConsumption();			
    double timeStepGreenEnergyProduction = (max(0.0,greenPowerProduction)) * (newTime - time);
    double timeStepEnergyConsumption = newCumulativeEnergyConsumption - cumulativeEnergyConsumption;
    double brownTimeStepEnergyConsumption = max(0.0, timeStepEnergyConsumption - timeStepGreenEnergyProduction);
    double timeStepRemainingGreenEnergy = max(0.0, timeStepGreenEnergyProduction - timeStepEnergyConsumption);		
    cumulativeBrownEnergyConsumption += brownTimeStepEnergyConsumption;	
    cumulativeEnergyConsumption = newCumulativeEnergyConsumption;
    cumulativeGreenEnergyProduction += timeStepGreenEnergyProduction;
    cumulativeRemainingGreenEnergy += timeStepRemainingGreenEnergy;				
    double timeStepPowerConsumption = timeStepEnergyConsumption / (newTime - time);		
    time = newTime;			
    logResult();		
    double powInOut = greenPowerProduction;                    
    double powIn = greenPowerProduction;
    double out = powInOut - powIn;
    double powOut = greenPowerProduction + out;		
    logPowerResult(timeStepPowerConsumption, powInOut, powOut, powIn, out);
}

/**
 * Return the power currently available for the local consumption.
 * It is equals to: local_production + input_migration - output_migration
 * @return The current production of the data-center.
 */
double ElectricDC::getCurrentPowerProduction(){		
    return max(0.0, getAvailablePower());
}

/**
 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
 * (called by the master at the end of the simulation).
 */
void ElectricDC::stop(){
    try {
        power_output->close();
    } catch (const std::exception& e) 
    {            
        cout << "Error at ELECTRIC DC CLOSE : " << simgrid::s4u::this_actor::get_name() << " " << e.what()   << endl;
    }    	
}

string ElectricDC::getName(){
    return simgrid::s4u::this_actor::get_cname();
}

/**
 * Log the current power trajectories of the system in the second output file (format = time;consumption;production+In-Out;production-Out;production+In;-Out).
 */
void ElectricDC::logPowerResult(double consumption, double productionInOut, double productionOut, double productionIn, double out) {
    string separator = ";";//MasterVMManager.FILE_SEPARATOR;
    try {
        string log = std::to_string(time) 
                    + separator 
                    + std::to_string(consumption)
                    + separator
                    + std::to_string(productionInOut)
                    + separator
                    + std::to_string(productionOut)
                    + separator
                    + std::to_string(productionIn)
                    + separator
                    + std::to_string(out);
   //     //XBT_INFO(log.c_str());
        *power_output << log << endl;
    } catch (const std::exception& e) 
    {            
        cout << "Error : at ELECTRIC DC LOG POWER RESULT " << " " << e.what()   << endl;
    }	
}


void ElectricDC::getEnergyInformation()
{

     ////XBT_INFO("MANDOU MSG NO GRID GET ENERGY");
     EnergyInformation *info =new EnergyInformation(cumulativeBrownEnergyConsumption,
                                                    cumulativeEnergyConsumption,
                                                    cumulativeGreenEnergyProduction,
                                                    cumulativeRemainingGreenEnergy,
                                                    id);
                                                        
    MailBoxSingleton::GetInstance()->sendMessage("energy_info"+id,
        new Message(MESSAGE_GREEN_ENERGY_VALUE,info));

     
}

void ElectricDC::shutDownHosts()
{ 
  std::vector<simgrid::s4u::Host*> allHosts = getHosts();
  for(simgrid::s4u::Host* host : allHosts)
  {    
      if(host!= simgrid::s4u::this_actor::get_host())
      {                                
        host->set_pstate(1);
        host->turn_off();        
       }
  }
}


void ElectricDC::sendGreenEnergyInfo(){
  std::pair<std::string*,double> *values = new std::pair<std::string*,double> ;        
  values->first = new std::string(this->getID());
  values->second = getLocalPowerProduction();  
  MailBoxSingleton::GetInstance()->sendMessage("manager",new Message(MESSAGE_GREEN_ENERGY_VALUE,values));

}



void simulate_bootup(simgrid::s4u::Host* host, string id, std::vector<VMRequest *> *vms)
{

  host->set_pstate(2);
  host->turn_on();
  MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    
  simgrid::s4u::this_actor::sleep_for(150);  
  host->set_pstate(0);        
  MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    
  // The actor Needs to be created again instead of using the auto_restart, because when auto_restart is enabled,
  // as soon as the commad turn_on is executed the actor will start executing again, however we need it to start
  // only after the boot period (150 seconds in our case)
  simgrid::s4u::Actor::create("worker_"+host->get_name(), host,WorkerActor(host->get_name(),id));   

  for(VMRequest *vm : *vms){
    //XBT_INFO("ESSA VM VAI SER ENVIADA %s",vm->getName().c_str());    
    MailBoxSingleton::GetInstance()->sendMessage("worker_"+host->get_name(),new Message(MESSAGE_START_VM_EXECUTION, vm));
  }
  
  vms->clear();

  
}

static void simulate_shutdown(simgrid::s4u::Host* host)
{
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    
    host->set_pstate(1);
    simgrid::s4u::this_actor::sleep_for(10);
    host->turn_off();
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    
}


void ElectricDC::turnHostOn(simgrid::s4u::Host* host, VMRequest *vm){        
    //XBT_INFO("TURNING HOST ON");    
    
    vmQueues[host->get_name()];
    vmQueues[host->get_name()].push_back(vm);
    if(vmQueues[host->get_name()].size() == 1 )
    {
        simgrid::s4u::Actor::create("boot_"+host->get_name(), simgrid::s4u::this_actor::get_host(), simulate_bootup,host, this->getID(),&(vmQueues[host->get_name()]));
    }            
}

void ElectricDC::shutDownHost(simgrid::s4u::Host* host){        
    simgrid::s4u::Actor::create("shutdown_"+host->get_name(), simgrid::s4u::this_actor::get_host(), simulate_shutdown,host);
    WorkerInformation *wi = new WorkerInformation(host->get_core_count(),2000.00, host->get_name(),id, false,nullptr);        
    MailBoxSingleton::GetInstance()->sendMessage("manager",new Message(MESSAGE_HOST_INFORMATION,wi));
}



std::string ElectricDC::getID(){
    return id;
}

void ElectricDC::handleShutdownHost(void *data)
{
    std::string* hostName = static_cast<std::string*>(data);
    simgrid::s4u::Host *h = simgrid::s4u::Host::by_name(*hostName);    
    shutDownHost(h);        

}