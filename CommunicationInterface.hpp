#ifndef COMMUNICATIONINTERFACE_HPP
#define  COMMUNICATIONINTERFACE_HPP
#include <string>
#include "MailBoxSingleton.hpp"
#include "Message.hpp"
class CommunicationInterface
{
    public:
        virtual void handleMessage(Message *message) = 0;
        virtual void registerObject(std::string name,CommunicationInterface *object ){
            MailBoxSingleton::GetInstance()->registerObject(name,object);
        }
};
#endif