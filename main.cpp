/* Copyright (c) 2010-2020. The SimGrid Team. All rights reserved.          */
/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include <simgrid/s4u.hpp>
#include "simgrid/plugins/energy.h"
#include "xbt/replay.hpp"
#include "xbt/str.h"
#include <boost/algorithm/string/join.hpp>
#include "VMDispatcher.hpp"
#include "Manager.hpp"
#include "ElectricGrid.hpp"
#include "simgrid/plugins/live_migration.h"


XBT_LOG_NEW_DEFAULT_CATEGORY(simulationMain, "Main Logger");

int main(int argc, char* argv[])
{ 
  /*Inits the energy plugin*/   
  sg_vm_live_migration_plugin_init();
  sg_host_energy_plugin_init();

  simgrid::s4u::Engine e(&argc, argv);
  /* Register the classes representing the actors */
  e.register_actor<VMDispatcher>("vmdispatcher");/* Submits VMs */
  e.register_actor<ElectricGrid>("electricgrid");  /* Logs the energy consumption of the data centers */
  e.register_actor<Manager>("manager");/* Handles VM requests and schedules them */
  /* Load the platform description and how it will be deployed */
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
 /* argv[3] is the trace file */
  if (argv[3]) {
    /* loads the trace file */
    xbt_replay_set_tracefile(argv[3]);
   // simgrid::xbt::action_fs = new std::ifstream(argv[3], std::ifstream::in);
  }  
  /* Register the class and methods that will read the trace file */
  xbt_replay_action_register("sendVM", VMDispatcher::sendVM);
  xbt_replay_action_register("stop", VMDispatcher::stop);



  /* Run the simulation */
  e.run();


  XBT_INFO("Simulation is over");    
  return 0;

}