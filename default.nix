{
  pkgs ? import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) {}
}:

with pkgs;

let
  packages = rec {

    # The derivation for chord
    simulationmain = stdenv.mkDerivation rec {
      pname = "simulationmain";
      version = "0.1.0.1.in-tuto";
      src = fetchgit {
        url = "https://gitlab.com/migvasc/nix-teste/";
        rev = "2c10c9c6e85345c95a5300d45feda4969b4d7842 ";
        sha256 = "0iwr7zd1yl8pcnpdqfk50rs7jb08qswc8rzf40zfca4r71z9vrxi";
      };

      buildInputs = [
        pkgconfig
        simgrid
        boost
        cmake
      ];

    };

    # The shell of our experiment runtime environment
    expEnv = mkShell rec {
      name = "exp01Env";
      buildInputs = [
        simulationmain
      ];
    };

  };
in
  packages
