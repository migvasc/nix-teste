#include "EnergyInformation.hpp"

EnergyInformation::EnergyInformation(double _cumulativeBrownEnergyConsumption,
                    double _cumulativeEnergyConsumption,
                    double _cumulativeGreenEnergyProduction,
                    double _cumulativeRemainingGreenEnergy,
                    std::string dc){
        
        cumulativeBrownEnergyConsumption = _cumulativeBrownEnergyConsumption;
        cumulativeEnergyConsumption = _cumulativeEnergyConsumption;
        cumulativeGreenEnergyProduction = _cumulativeGreenEnergyProduction;
        cumulativeRemainingGreenEnergy = _cumulativeRemainingGreenEnergy;
        dcName = dc;
    }

std::string EnergyInformation::getDCName(){
    return dcName;
}

double EnergyInformation::getCumulativeBrownEnergyConsumption()
{
    return cumulativeBrownEnergyConsumption;

}
double EnergyInformation::getCumulativeEnergyConsumption()
{
    return cumulativeEnergyConsumption;
}
double EnergyInformation::getCumulativeGreenEnergyProduction()
{
    return cumulativeGreenEnergyProduction;
}
double EnergyInformation::getCumulativeRemainingGreenEnergy()
{
    return cumulativeRemainingGreenEnergy;
}        
