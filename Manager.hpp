#ifndef MANAGER_HPP
#define MANAGER_HPP

#include <simgrid/s4u.hpp>
#include <queue> 
#include "VMRequest.hpp"
#include "Message.hpp"
#include "ElectricDC.hpp"
#include "PVActor.hpp"
#include "WorkerInformation.hpp"
#include "MailBoxSingleton.hpp"
#include "CommunicationInterface.hpp"
#include "EnergyInformation.hpp"
#include <limits>
#include <math.h>  
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include "VMInterface.hpp"
#include "MigrationPlan.hpp"
#include <chrono>
#include "robin_hood.h"

using namespace std::chrono; 
class Manager : CommunicationInterface {
private:
  long tasks_count              = 0;
  int compute_cost              = 0;
  double communicate_cost       = 0;  

	/**
	 * The bandwidth available on one route to migrate the VMs (considered to be the same for all the routes).
	 */
	// TODO: we divide by 1.05 because of a simgrid bug with cross traffic. Remove when fixed. 
	double bandwidth = 1000000000./(8. * 1.05);			
	/**
	 * The minimum latency to go from on DC to another one (indexed by source and then by destination).
	 */
	double maxInterDClatency  = 9 * 0.00225;	
	/**
	 * The maximum latency to go from on DC to another one (indexed by source and then by destination).
	 */
	double minInterDClatency = 5 * 0.00225;			
	/**
	 * The latency to go from a server to another one in the same DC.
	 */
	double intraDClatency = 2 * 0.00225;	
	/**
	 * Arbitrary parameter of SimGrid for computing TCP latency.
	 */
	double gamma = 20537;	
  /**
	 * The size of the TCP window.
	 */
	double windowSize = 4194304;

	/**
	 * Arbitrary parameter of SimGrid for computing migration time.
	 */
	double alpha = 13.01;
	/**
	 * Arbitrary parameter of SimGrid for computing logical bandwidth.
	 */
	double bandwidthRatio = 0.97;


	/**
	 * The power cost of using a vCPU.
	 */
	double pcore = 20.5;

  template<typename Base, typename T>
  inline bool instanceof(const T*) {
    return is_base_of<Base, T>::value;
  }  


// #### SCORPIOUS PARAMS

	/**
	 * Cost of buying energy (in euros/kWh)
	 */
	double buying_cost= 0.15 ;
	
	/**
	 * Gain of selling energy (in euros/kWh).
	 */
	double selling_gain= 0.06;
	
	/**
	 * Cost of negative auto-consomation gap (in euros/MWh).
	 */
	double autoconso_cost = 63.63;
	
	/**
	 * Gain of positive auto-consomation gap (in euros/MWh).
	 */
	double autoconso_gain= 54.21;
	
	/**
	 * Price of power network utilisation at peak hours (in euros/kWh).
	 */
	double fullUtilisationCost= 0.0418;
	
	/**
	 * Price of power network utilisation at off-peak hours (in euros/kWh).
	 */
	double lightUtilisationCost= 0.0281;
	
	/**
	 * Duration of peak time (in sec).
	 */
	double fullCostPeriod= 16.0 * 3600;
	
	/**
	 * Duration of off-peak time (in sec).
	 */
	double lightCostPeriod= 8.0 * 3600;
	
	/**
	 * Period of auto-conso paying (in sec).
	 */
	double autoconsoPeriod  = 30.0 * 60;
	
	/**
	 * Period of energy trading (in sec). 
	 */
	double sellBuyPeriod= 10.0 * 60;

	/**
	 * If true, we try to minimize the global financial cost of the cloud (i.e. SCORPIUS algorithm),
	 * else we try to minimize brown energy consumption of the DC (i.e. NEMESIS).
	 */
	bool optimizeFinancialCost = false;


	/**
	 * The maximum number of time-slot evaluated
	 */
	int n_eval = 20;

  //Timeslot duration
  int timeslot = 300;



	/**
	 * The duration a host takes to power ON
	 */
	double hostPoweringOnDelay = 150;
	
	/**
	 * The duration a host takes to power OFF
	 */
	double hostPoweringOffDelay = 10;
	


  //Stores the DC name and their respective green energy production
  robin_hood::unordered_map<std::string, double> dcsGreenProduction;
  robin_hood::unordered_map<std::string, double> dcStdDevMap;
  robin_hood::unordered_map<std::string, double> dcEnergyStdDevMap;


  robin_hood::unordered_map<double, robin_hood::unordered_map<std::string,double>> dcsIdealPow;


  //Stores the server name and their respective informations (available cores, available RAM and whether it is on)
  robin_hood::unordered_map<std::string, WorkerInformation*> serversInformation;
  robin_hood::unordered_map<std::string, std::vector<WorkerInformation*>> dcWorkersInfo;
  
  //Stores the servers of the data centers
  robin_hood::unordered_map<std::string, std::vector<std::string>> dcServers;
  std::vector<std::string> dcsName;
  std::vector<ElectricDC *> dcs;

  std::queue<Message*> mqueue;
  double maxSimDuration = 3000;
  std::queue <VMRequest*> vmRequestQueue;  
  std::vector<std::string> argsClass;
  std::vector<VMRequest*> vms;
  robin_hood::unordered_map<std::string, std::map<double,double> > consumptions_historic;
  std::vector<VMRequest *> delayedvms;
  std::vector<MigrationPlan*> migrationsList;



//WHERE THOSE VALUES CAME FROMM??? - based on paper taurus node grid 5k
  double p_idle = 97.0;
  double p_down = 8.0;
  double p_on = 127.0;
  double p_off = 200.0;
  double p_core = 20.5;
  int nbPeriod = 0;
  int stdDevPeriod = 24 * 60 * 60;

  // #### Nemesis attributes #### 
  /**
	 * The preallocations previously made.
	 */
  robin_hood::unordered_map<VMRequest*, std::string> preallocations;
	/**
	 * The power consumption of each DC considering the preallocations previously made.
	 */  
  robin_hood::unordered_map<std::string, double> powerConsumptions;

	/**
	 * The historic (in the format {(time,power)}*) of the green power production of each DC.
	 * Used to compute the standard deviation of the green power productions.
	 */  
  robin_hood::unordered_map<std::string, std::map<double,double>>   greenPower;

  /**
	 * The ideal green power production trajectory (format {(time,power)*).
	 * Used to scale the actual power production.
	 */
	std::map<double,double> idealGreenPow;

  /**
   * Number of solar panels per data center
   * 
   */
  robin_hood::unordered_map<std::string,double> nbPv;


  robin_hood::unordered_map<std::string,  std::map<double,std::vector<VMInterface*> > > dcVMExtinctionTimes;
  robin_hood::unordered_map<std::string,  std::map<double,int> > dcHostsExtinctionTimes;

  void handleVMSubmission(VMRequest* vm);
  void handleHostInformation(void* data );
  void handleGreenEnergyValue(Message* message);
  void performSchedule();
  void handleMessage(Message *message);
  void shutDownHosts();
  void init();
  void updateState();
  void nemesisBehavior();
  void updateConsumptionsHistoric();
  void updateGreenPow();
  void allocateVm();
  double powerConsumption(const std::string& dc);
  std::vector<std::string> getServersWithMinAvailableVolume(const int& vCPU);
  std::vector<std::string> getServersWithMinAvailableVolume(const std::string& dc,const bool& mustBeOn,const int& vCpu);
  std::vector<std::string> getSortedServers(const bool& increasing);
  int nbAvailableCoresWithPreallocationsAndMigrations(const std::string& worker);
  int nbAvailableCoresWithPreallocationsAndMigrations(WorkerInformation* worker);
  double expectedBrownVmAdditionCost(const std::string& worker, VMRequest *vm);
  double vmAdditionCost(const std::string& worker, VMRequest *vm);
  bool isWorkerOffWithPreallocationsAndMigrations(const std::string& worker);
  double expectedBrownPowerUntilSunset(const std::string& dc, double P,const double& t_max);
  double standardDeviation (const std::string& dc);
  double energyStandardDeviation(const std::string& dc);
  double avg(const std::string& dc);
  double getIdealPowerAtTime(double time, const std::string& dc);  
  void loadIdealPowerTrajectory(const std::string& idealGreenPowPath);
  void getVmExtinctionTimes(const std::string& dc);
  void  getHostsExtinctionTimes(const std::string& dc,const std::map<double,std::vector<VMInterface*>>& vmExtinctionTimes);
  double expectedBrownPower(const std::string& dc,const double& P,const double& t,const double& standardDev);
  double expectedGreenPow(const std::string& dc,const double& t);  
	std::map<std::string,double> gap;
  double Phi(const double& x,const double& greenPow,const double& standardDev);
  double phi(const double& x,const double& greenPow,const double& standardDev);
  void deployPreallocations();
  double erf(const double& z);  
  void migratePreallocations();
  std::vector<std::pair<std::string, double>> sorteDcByERGE();
  double expectedRemainingGreenPow(const std::string& dc, double P);
  double expectedRemainingGreenPow(const std::string& dc, const double& P,const double& stdDev,const double& t);
  void migrateRunningVM();
  unordered_map<std::string,std::vector<DeployableVM*>>  buildListOfVmToMigrate();
  double computeMigrationTime(DeployableVM *vm, const bool& interDC,const bool& minLatency);
  double *planVmMigrationsForDCs(const std::string& dcSending,const std::string& dcReceiving, std::vector<DeployableVM*>& listToMigrateSending,  double& tmin,double& tmax, double *times);
  double *computeTminTmaxTsend( double tmin,  double tmax);
  double costOfVm(DeployableVM *vm);
  void printPreAllocations();
  void startMigrations();
  void prepareForNemesis();
  void consolidateDC(std::string site);
  std::vector<VMInterface*> buildNonMigratingVMSortedList(const std::vector<std::string>& dcServers, int& k);
  WorkerInformation* searchLastPossibleServerForVM(VMInterface *vm, std::vector<std::string> dcServers, int k) ;
  void shutDownEmptyHosts();
  double expectedFinancialCostsUntilSunset(double *P,const double& t_max,const double& EconsumptionInit);
  double computeCostComputationMaxBound(const double& time,const double& t_max);
  double computeGlobalCostsUntilSunrise(double t_min,const double& t_max, double *P,const double& standardDev,const double& aPeriod,const double& aGain,const double& aCost,const double& totalE_init,const double& scale,double* standardDev_dc);
  double computeGlobalCost(const double& consumption,const double& production,const double& standardDev,const double& cost,const double& gain,const double& scale);
  double expectedBrownEnergy(const std::string& dc,const double& P,const double& greenPow,const double& t,const double& standardDev) ;
public:
  explicit Manager(std::vector<std::string> args);
  void operator()();

};
#endif