#include "WorkerInformation.hpp"
#include "DeployableVM.hpp"

WorkerInformation::WorkerInformation(int cores, double RAM,std::string name,std::string dc, bool on, robin_hood::unordered_map<std::string, DeployableVM*> *vms)
{
    availableCores = cores;
    availableRAM = RAM;
    isOn = on;
    hostName = name;
    dcName=dc;
    runningVMS = vms;
}

int WorkerInformation::getAvailableCores()
{
    return availableCores;
}
double WorkerInformation::getAvailableRAM() {
    return availableRAM;
}
bool WorkerInformation::serverIsOn() {
    return isOn;
}

std::string *WorkerInformation::getHostName() {
    return &hostName;
}

std::string *WorkerInformation::getDCName() {
    return &dcName;
}


void WorkerInformation::setAvaliableCores(int cores){
    availableCores = cores;
}

void WorkerInformation::setAvaiableRAM(double RAM){
    availableRAM = RAM;
}

robin_hood::unordered_map<std::string, DeployableVM*> *WorkerInformation::getRunningVMS(){
    return runningVMS;
}

void WorkerInformation::setIsOn(bool on)
{
    isOn = on;
}
