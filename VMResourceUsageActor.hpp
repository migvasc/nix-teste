#ifndef VMRESOURCEUSAGEACTOR_HPP
#define VMRESOURCEUSAGEACTOR_HPP

#include <simgrid/s4u.hpp>
#include "DeployableVM.hpp"

class VMResourceUsageActor 
{

private:

DeployableVM *vm;
std::vector<std::pair<int,int>> resource_usage_history;
unsigned long long original_ram_size;
double original_cpu_bound;

public:

explicit VMResourceUsageActor();
explicit VMResourceUsageActor(DeployableVM *aVM,std::vector<std::pair<int,int>> history );
void operator()();


};
#endif