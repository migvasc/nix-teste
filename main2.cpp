#include <simgrid/s4u.hpp>
#include <queue> 
#include "VMRequest.hpp"
#include "Message.hpp"
#include "ElectricDC.hpp"
#include "PVActor.hpp"
#include "WorkerInformation.hpp"
#include "MailBoxSingleton.hpp"
#include "CommunicationInterface.hpp"
#include "EnergyInformation.hpp"
#include <limits>
#include <math.h>  
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include "VMInterface.hpp"

int main(){

    cout << "OI" << endl;
    std::map<int, std::string> mapa;
    mapa.emplace(1,"a");
    mapa.emplace(2,"b");
    mapa.emplace(3,"c");
    mapa.emplace(4,"d");
    mapa.emplace(5,"e");

    int a = mapa.begin()->first;
    int b;

    std::map<int,string>::iterator it = mapa.begin();
    std::map<int,string>::iterator it2 = std::next(it);
    cout << "it " << it->first << endl;
    cout << "it2 " << it2->first << endl;
    ++it;
    ++it2;
    cout << "it " << it->first << endl;
    cout << "it2 " << it2->first << endl;

}