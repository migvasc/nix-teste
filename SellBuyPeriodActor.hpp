#ifndef SELLBUYPERIODACTOR_HPP
#define SELLBUYPERIODACTOR_HPP

#include <simgrid/s4u.hpp>
#include "MailBoxSingleton.hpp"
#include "FinancialSystem.hpp"
#include "CommunicationInterface.hpp"
#include <math.h>  

class SellBuyPeriodActor : CommunicationInterface 
{

private:

double updateTime;
void init();
FinancialSystem *financial =nullptr;
public:

explicit SellBuyPeriodActor();
explicit SellBuyPeriodActor(double anUpdateTime, FinancialSystem *aFinancialSystem);
void operator()();
void handleMessage(Message *message);


};



#endif