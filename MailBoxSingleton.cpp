#include "MailBoxSingleton.hpp"
#include "CommunicationInterface.hpp"

/**
 * Static methods should be defined outside the class.
 */

MailBoxSingleton* MailBoxSingleton::pinstance_{nullptr};
std::mutex MailBoxSingleton::mutex_;

/**
 * The first time we call GetInstance we will lock the storage location
 *      and then we make sure again that the variable is null and then we
 *      set the value. RU:
 */
MailBoxSingleton *MailBoxSingleton::GetInstance()
{
    
    std::lock_guard<std::mutex> lock(mutex_);
    if (pinstance_ == nullptr)
    {
        
        pinstance_ = new MailBoxSingleton();        
    }
    

    return pinstance_;

}

void MailBoxSingleton::sendMessage(std::string mailboxID,Message *message)
{
    try {           
        if(messageHash.find(mailboxID) == messageHash.end()){
            messageHash[mailboxID] = new std::queue<Message*>;
        }    
        messageHash[mailboxID]->push(message);
        
        if(messageObj.find(mailboxID)!= messageObj.end()){
            
            messageObj[mailboxID]->handleMessage(message); 
            messageHash[mailboxID]->pop();

        }
    } catch (const std::exception& e) 
    {            
        cout << "Error : at mailbox singleton send message   "   << e.what() << endl;

    }	
}

Message *MailBoxSingleton::getMessage(std::string id)
{
    Message * result = nullptr;
    
    if(!messageHash.empty() && messageHash.find(id) != messageHash.end() && !messageHash[id]->empty()){
        result = messageHash[id]->front();
        messageHash[id]->pop();
    }
    return result;
}

void MailBoxSingleton::registerObject(std::string name,CommunicationInterface *object ){
    messageObj[name] = object;
}

