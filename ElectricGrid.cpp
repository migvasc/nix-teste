#include "ElectricGrid.hpp"
#include "simgrid/plugins/energy.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(egrid, "electric grid category");

ElectricGrid::ElectricGrid(std::vector<std::string> args) : ElectricSystem("output/global_energy.csv") 
{   
    argsClass = args;
}

void ElectricGrid::operator()()
{
    
    void* data = nullptr;
    Message *message;
    registerObject("grid",this); 
    mutex = simgrid::s4u::Mutex::create();
    financial = new FinancialSystem(aBuying_cost, aSelling_gain, anAutoconso_cost, anAutoconso_gain, simgrid::s4u::this_actor::get_host(), aFullUtilisationCost, aLightUtilisationCost, aFullCostPeriod, aLightCostPeriod, autoconsoPeriod, sellBuyPeriod);           
    int nDCs =std::stoi(argsClass[1]);    
    simgrid::s4u::Host *host = simgrid::s4u::this_actor::get_host();
    
    for(int i =0 ;i<nDCs;i++)
    {                    
        dcs.push_back(argsClass[i+2]);
        simgrid::s4u::Actor::create(argsClass[i+2], host,ElectricDC(argsClass[i+2],std::stoi(argsClass[i+2+nDCs]),argsClass[i+2+nDCs+nDCs]));
    }

    while(true)
    {
        simgrid::s4u::this_actor::sleep_for(100.0);
    }    
}

void ElectricGrid::handleMessage(Message* message)
{
mutex->lock();
 switch (message->type)
    {
        
        case MESSAGE_UPDATE_ENERGY:            
          //  //XBT_INFO("RECEBEU MSG UPDATE ENERGY");
            updateEnergy(round(simgrid::s4u::Engine::get_clock()));
            break;                        
        case MESSAGE_GRID_GET_ENERGY_INFO:
            handleGetGridEnergyInfo();
            break;
        default:
            break;
    }
mutex->unlock();
}

void ElectricGrid::updateEnergy(double newTime)
{
    
    sg_host_energy_update_all();        
    if(newTime!=lastTime){
        lastTime = newTime;    
        
        for(std::string dc : dcs)
        {
        //    //XBT_INFO("MANDOU PARA O DC %s MESSAGE_UPDATE_ENERGY",dc.c_str());
            MailBoxSingleton::GetInstance()->sendMessage(dc,new Message(MESSAGE_UPDATE_ENERGY) );
        }

        
        cumulativeBrownEnergyConsumption = 0;
        cumulativeEnergyConsumption = 0;
        cumulativeGreenEnergyProduction = 0;
        cumulativeRemainingGreenEnergy = 0;
            
    
        for(std::string dc : dcs)
        {
          //  //XBT_INFO("MANDOU PARA O DC %s MESSAGE_GET_ENERGY",dc.c_str());
            MailBoxSingleton::GetInstance()->sendMessage(dc,new Message(MESSAGE_GET_ENERGY) );
        }
        
     //   //XBT_INFO("ESPERANDO RECEBER AS MSGSSS");
        Message *msg;
        EnergyInformation* info;
        for(std::string dc : dcs)
        {                                     
            msg = MailBoxSingleton::GetInstance()->getMessage("energy_info"+dc);
            if(msg!= nullptr)
            {

                info = static_cast<EnergyInformation*>(msg->data);                
                cumulativeBrownEnergyConsumption += info->getCumulativeBrownEnergyConsumption();
                cumulativeEnergyConsumption += info->getCumulativeEnergyConsumption();
                cumulativeGreenEnergyProduction += info->getCumulativeGreenEnergyProduction();
                cumulativeRemainingGreenEnergy += info->getCumulativeRemainingGreenEnergy();

        // TODO VALIDATE IF DELETE CAN F WITH ANYTHING OF THE CODE      
        //      delete info;

            }        
        }

        /*
                (double _cumulativeBrownEnergyConsumption,
                        double _cumulativeEnergyConsumption,
                        double _cumulativeGreenEnergyProduction,
                        double _cumulativeRemainingGreenEnergy,        
        */
      //  //XBT_INFO("RECEBEU AS MSGSSS");
        time = round(simgrid::s4u::Engine::get_clock());
        logResult();      
    }
    
}


void ElectricGrid::handleGetGridEnergyInfo()
{
    EnergyInformation* grid_energy_info = new EnergyInformation(cumulativeBrownEnergyConsumption,cumulativeEnergyConsumption,cumulativeGreenEnergyProduction, cumulativeRemainingGreenEnergy,"grid");
    MailBoxSingleton::GetInstance()->sendMessage("grid_energy_info",
        new Message(MESSAGE_GREEN_ENERGY_VALUE,grid_energy_info));

}
