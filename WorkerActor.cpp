#include "WorkerActor.hpp"
#include "DeployableVM.hpp"
#include "simgrid/plugins/live_migration.h"
#include "MigrationPlan.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(workerActor, "workeractor category");


static void compute()
{
    
    simgrid::s4u::this_actor::execute(8024000000.0 * 100000);                  
      simgrid::s4u::ExecPtr ptr = simgrid::s4u::this_actor::exec_init(8024000000.0 * 100000);                  
  ptr->cancel();
}


static void vm_migrate(WorkerActor *parent, MigrationPlan * plan)
                       
{        

    DeployableVM* vm = plan->getVM();
    simgrid::s4u::Host* dst_pm =plan->getHost();
    const double& startTime = plan->getTime();
    const simgrid::s4u::Host* src_pm = vm->getVM()->get_pm();      

    if (simgrid::s4u::Engine::get_clock() < startTime)
    {
        simgrid::s4u::this_actor::sleep_for(startTime -simgrid::s4u::Engine::get_clock() );
    }
            
    
    vm->setIsVMMigrating(true);    
    sg_host_energy_update_all();         
    
    MailBoxSingleton::GetInstance()->sendMessage("worker_"+dst_pm->get_name(),new Message(MESSAGE_VM_MIGRATION_STARTED,plan));
    double mig_sta = simgrid::s4u::Engine::get_clock();
    sg_vm_migrate(vm->getVM(), dst_pm); 
    double mig_end =simgrid::s4u::Engine::get_clock(); 
    cout << simgrid::s4u::Engine::get_clock() <<";"<< vm->getName() <<";" << src_pm->get_name() << ";" <<dst_pm->get_name()<< ";" << mig_sta << ";" <<  mig_end << ";" <<  (mig_end - mig_sta) << ";" <<  plan->getWorstMigrationTime() << ";" << plan->getBestMigrationTime() << endl;
    sg_host_energy_update_all();               

           
    vm->getWorkerActor()->removeRunningVMFromMemory(vm->getVM()->get_name());
    vm->getWorkerActor()->sendUpdatedWorkerInformation(src_pm->get_name());
    
    MailBoxSingleton::GetInstance()->sendMessage("worker_"+dst_pm->get_name(),new Message(MESSAGE_VM_MIGRATION_FINISHED,vm));

}


WorkerActor::WorkerActor(std::string name, std::string dc)
{
    serverName = name;
    dcName = dc;
}

void WorkerActor::operator()()
{
    registerObject(simgrid::s4u::this_actor::get_name(),this);
    //XBT_INFO("TO ON");        
    while(true)
    {                
        simgrid::s4u::this_actor::sleep_for(1000);        
    }   
    //XBT_INFO("Exiting now.");
}

void WorkerActor::removeRunningVMFromMemory(std::string vmID)
{
  runningVMS.erase(vmID);
}

void WorkerActor::handleExecutionFinished(std::string vmID)
{
    //XBT_INFO("HANDLE EXEC FINISHED %s",vmID.c_str());
    DeployableVM* vm;
    std::string hostName;
    if(!runningVMS.empty() && runningVMS.find(vmID) != runningVMS.end()){
        vm = runningVMS[vmID];
        hostName = vm->getVM()->get_pm()->get_name();        
        removeRunningVMFromMemory(vmID);
        //XBT_INFO("DELETING VM!!! %s",vm->getVM()->get_cname());
        sendUpdatedWorkerInformation(hostName);
        vm->getVM()->destroy();                
    }    
}


int WorkerActor::getAvailableCores(std::string hostName)
{        
    simgrid::s4u::Host * host = simgrid::s4u::Host::by_name(hostName);
    const simgrid::s4u::VirtualMachine* vm = dynamic_cast<simgrid::s4u::VirtualMachine*>(host);
    if (vm) // If it's a VM, take the corresponding PM
        host = vm->get_pm();

    int result = host->get_core_count();
    //XBT_INFO("CORE COUNT DO HOST %s  %d",simgrid::s4u::this_actor::get_host()->get_cname(), result);
    for (auto i = runningVMS.begin(); i != runningVMS.end(); i++) 
    {
        result = result - i->second->getVM()->get_core_count() *i->second->getcurrentCPUUsage();
        //XBT_INFO("VM %s RODANDO TEM QTD CORES : %d",i->first.c_str(), i->second->getVM()->get_core_count());
    }
    //XBT_INFO("CORES LIVRES %d",result);
    return result;
}

void WorkerActor::sendUpdatedWorkerInformation(std::string hostName)
{    
    int availableCOres = getAvailableCores(hostName);
    //XBT_INFO("SERVER TEM ISSO DE CORES %d", availableCOres);
    WorkerInformation *wi = new WorkerInformation(availableCOres,2000.00, hostName,this->getDCName(), true,&runningVMS);
    MailBoxSingleton::GetInstance()->sendMessage("manager",new Message(MESSAGE_HOST_INFORMATION,wi));
    //XBT_INFO("MENSAGEM ENVIADA PARA O MASTER DE UPDATE DO HOST");
}


void WorkerActor::handleMessage(Message *message)
{           
    switch (message->type)
    {
        case MESSAGE_START_VM_EXECUTION:
          startExecutingVM(static_cast<VMRequest *>(message->data));
          break;
        case MESSAGE_START_VM_MIGRATION:
          startMigratingVM(message->data);
          break;
        case MESSAGE_VM_MIGRATION_STARTED:
          handleVMMigrationStarted(static_cast<MigrationPlan*>(message->data));          
          break;
        case MESSAGE_VM_MIGRATION_FINISHED:
          handleVMMigrationFinished(static_cast<DeployableVM*>(message->data));          
          break;
        default:
          break;
    }
    delete message;
}

void WorkerActor::startExecutingVM(VMRequest* vm)
{                    
    //XBT_INFO("This VM will be started %s cores %d",vm->getName().c_str(),vm->getNbCores());            
    DeployableVM* dvm = new DeployableVM(vm->getName(),simgrid::s4u::Host::by_name(vm->getDestHost()),vm->getNbCores(),vm->getRamSize(), vm->getFlopAmount(),this);            
    runningVMS[dvm->getVM()->get_name()]=dvm;    
    dvm->startVM();
    sendUpdatedWorkerInformation(dvm->getVM()->get_pm()->get_name());

    if(vm->getVMResourceUsageHistory().size()>0)
    {
        simgrid::s4u::Actor::create("usage_"+ vm->getName(), dvm->getVM(),VMResourceUsageActor(dvm,vm->getVMResourceUsageHistory()));
    }
    
    delete vm;
}

void WorkerActor::startMigratingVM(void *data)
{    
    MigrationPlan *migration = static_cast<MigrationPlan*>(data);
    DeployableVM *vm = migration->getVM();
    simgrid::s4u::Host* h =simgrid::s4u::Host::by_name(vm->getWorkerActor()->getServerName());
    //cout << "current host for migration " <<h->get_name() << endl;
    simgrid::s4u::Actor::create("mig_"+ vm->getName(), h,vm_migrate,this,migration);
}   

void WorkerActor::handleVMMigrationStarted(void *data)
{
    MigrationPlan *plan = static_cast<MigrationPlan*>(data);
    runningVMS[plan->getVM()->getVM()->get_name()] = plan->getVM();    
    sendUpdatedWorkerInformation(plan->getHost()->get_name());

}

void WorkerActor::handleVMMigrationFinished(DeployableVM *vm)
{
    //XBT_INFO("UPDATING VM");
    //cout << round(simgrid::s4u::Engine::get_clock()) << " MIGRATION FINISHED " <<vm->getName() << endl;
    runningVMS[vm->getVM()->get_name()] = vm;
    vm->setWorkerActor(this);
    vm->setIsVMMigrating(false);
    //cout << "ENERGY CONSUMPTION DEST AFTER MIG  " << std::to_string( sg_host_get_consumed_energy(simgrid::s4u::Host::by_name(this->getServerName())  )) << endl;

    sendUpdatedWorkerInformation(vm->getVM()->get_pm()->get_name());
  //  simgrid::s4u::Host *h = simgrid::s4u::Host::by_name("stremi-1.reims.grid5000.fr");
//    simgrid::s4u::Actor::create("mig2_"+ vm->getName(), h,vm_migrate,this,vm,h, (round(simgrid::s4u::Engine::get_clock()) + 10));

}

std::string WorkerActor::getDCName(){
    return dcName;
}
std::string WorkerActor::getServerName(){
    return serverName;
}
robin_hood::unordered_map<std::string, DeployableVM*> WorkerActor::getRunningVMS(){
    return runningVMS;
}