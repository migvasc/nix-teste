#ifndef PVPANEL_HPP
#define PVPANEL_HPP



#include <simgrid/s4u.hpp>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include <limits>
#include "MailBoxSingleton.hpp"
#include "Message.hpp"

using namespace std;

class PVPanel 
{
	/**
	 * The input file containing the power production trajectory.
	 */
    string inputFile;
	
    std::vector<std::pair<double,double>> greenPowerProduction;

	string dcName; 
	
	/**
	 * The next green power production according to the input trace.
	 */
	double nextGreenPowerProduction;
	
	/**
	 * The current green power production according to the input trace.
	 */
	double currentGreenPowerProduction;
	
	/**
	 * the last time the power production was updated.
	 */
	double time;
	
	/**
	 * the next time the power production will be updated.
	 */
	double nextTime;

    /**
	 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
	 */
	void advanceTime(double time);
	void loadCSVFile();
public:    
	explicit PVPanel();
    explicit PVPanel(std::string fileName);
	double getCurrentGreenPowerProduction(double time);
};

#endif