#include "UtilisationCostSwitchActor.hpp"

UtilisationCostSwitchActor::UtilisationCostSwitchActor(const double& aFullCost, const double& aLightCost, const double& aFullCostPeriod,const double& aLightCostPeriod,FinancialSystem *aFinancial)
{
    fullCost = aFullCost;
    lightCost = aLightCost;
    fullCostPeriod =aFullCostPeriod;
    lightCostPeriod = aLightCostPeriod;
    financial=  aFinancial;
}

void  UtilisationCostSwitchActor::operator()()
{
    registerObject("utilisationCostSwitchActor",this);
    while(true)
    {
        simgrid::s4u::this_actor::sleep_for(fullCostPeriod);
        MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));
        financial->updateSellBuy(round(simgrid::s4u::Engine::get_clock()));
        financial->updateUtilisation(round(simgrid::s4u::Engine::get_clock()));
        simgrid::s4u::this_actor::sleep_for(lightCost);
        MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));
        financial->updateSellBuy(round(simgrid::s4u::Engine::get_clock()));
        financial->updateUtilisation(round(simgrid::s4u::Engine::get_clock()));

    }
}

void UtilisationCostSwitchActor::handleMessage(Message *message)
{

}