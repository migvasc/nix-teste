#include "Manager.hpp"
#include "DeployableVM.hpp"
#include "simgrid/plugins/live_migration.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(manager, "manager category");
/**
 * Manages the distributed cloud with on-site green (e.g. solar) energy productions.
 * The cloud is composed of heterogeneous hosts. 
 * The manager is in charge of dispatching an incoming workload to the different data-centers.
 * Rather than directly interacting with the hosts, the manager deploy and interact with worker applications which encapsulate the hosts desired behavior. 
 * The workload is composed of VMs which require a given number of cores and has to be executed for a given period of time.
 * The master, and therefore the workload, have a time-stepped behavior.
 *
 */
Manager::Manager(std::vector<std::string> args)
{      
    argsClass = args;    
}

/* "main" method */
void Manager::operator()()
{      
    registerObject("manager",this);            
    init();
    while(true)
    {          
        auto start = high_resolution_clock::now();                       
        performSchedule(); 

        auto stop = high_resolution_clock::now(); 
        auto duration = duration_cast<nanoseconds>(stop - start); 

        simgrid::s4u::this_actor::sleep_for(300.0);                    

        
    }          
}

void Manager::init()
{
    simgrid::s4u::Host* host = simgrid::s4u::this_actor::get_host();    
    shutDownHosts();
    int nDCs =std::stoi(argsClass[1]);    
    for(int i =0 ;i<nDCs;i++)
    {
        nbPv[argsClass[i+2]] = std::stoi(argsClass[i+2+nDCs]);
        dcsName.push_back(argsClass[i+2]);
        gap[argsClass[i+2]]=0;
        greenPower[argsClass[i+2]];
    }
    loadIdealPowerTrajectory(argsClass[argsClass.size()-1]);    
}

void Manager::handleMessage(Message* message)
{
    if (message != nullptr)
    {            
        switch (message->type)
        {
            case MESSAGE_VM_SUBMISSION:
                handleVMSubmission(static_cast<VMRequest*>(message->data));        
                break;      
            case MESSAGE_HOST_INFORMATION:
                handleHostInformation(message->data);
                break;                                        
            case MESSAGE_GREEN_ENERGY_VALUE:
                handleGreenEnergyValue(message);
                break;
            default:
                break;
        }
        message = nullptr;
    }

}




void Manager::handleGreenEnergyValue(Message *message)
{
    std::pair<std::string*,double>* result;    
    result = static_cast<std::pair<std::string*,double>*>(message->data);                     
    dcsGreenProduction[*result->first] = result->second;                    
    delete result;
}

void Manager::handleHostInformation(void* data )
{
  WorkerInformation* wi = static_cast<WorkerInformation*>(data);
  ////XBT_INFO("HANDLE HOST INFORMATION");
  
  if(serversInformation.find(*wi->getHostName())==serversInformation.end())
  {
    dcServers[*wi->getDCName()].push_back(*wi->getHostName());
  }
  delete serversInformation[*wi->getHostName()];
  serversInformation[*wi->getHostName()] = wi;
  //cout.precision(17);
  //cout << "ENERGY CONSUMPTION host info  " << fixed << sg_host_get_consumed_energy(simgrid::s4u::Host::by_name(*wi->getHostName())  ) << endl ;
  if(wi->serverIsOn())
  {
    //cout << "TA LIGADO  " << fixed << sg_host_get_consumed_energy(simgrid::s4u::Host::by_name(*wi->getHostName())  ) << endl ;
  }
  //XBT_INFO("SERVER INFO RECEIVED host name %s free cores %d",serversInformation[*wi->getHostName()] ->getHostName()->c_str(),serversInformation[*wi->getHostName()]->getAvailableCores() );

}

void Manager::handleVMSubmission(VMRequest* vm)
{    
    compute_cost  = vm->getRamSize();            
    vmRequestQueue.push(vm);
    vms.push_back(vm);
}

/**
 * Value of the sorting strategy by decreasing volume (CPU cores).
 */
bool sortVMByCPU(VMInterface *a,  VMInterface *b)
{
    if(a->getNbCores() == b->getNbCores())
    {
        if(a->getRemainingDuration() == b->getRemainingDuration())
        {
            return a->getName().compare(b->getName())<0;
        }
        return a->getRemainingDuration()>b->getRemainingDuration();
    }
    else 
        return (a->getNbCores()>b->getNbCores());
}


/**
 * Record the green power production of each DC.
 * First, as we compute the stdDev basing only on a finite period of time (e.g. day) not on the entire history, we delete the history each new period of time. We keep the last received value. 
 * WARNING : we make the hypothesis that all the DC have the same hour. Hence, we reset all the histories each new day.	
 * Then we store the green power production of each DC in the historic and we update the gaps with the reference trajectory accordingly.
 */
void Manager::updateGreenPow()
{
    //XBT_INFO("updating green power %f",round(simgrid::s4u::Engine::get_clock()));
    // first we check if we need to delete the historic
    if(round(simgrid::s4u::Engine::get_clock()) > (nbPeriod+1)*stdDevPeriod )
    {
        nbPeriod++;
        for(auto greenPowerItem = greenPower.begin() ; greenPowerItem != greenPower.end() ; ++greenPowerItem )
        {
            greenPowerItem->second.clear();            			
        }
    }
    
    for(std::string dc : dcsName)
    {         
        MailBoxSingleton::GetInstance()->sendMessage(dc,new Message(MESSAGE_GET_GREEN_ENERGY) );                    
    }
    // then we update the green power productions and gaps.
    for(auto greenPowerItem = greenPower.begin() ; greenPowerItem != greenPower.end() ; ++greenPowerItem )
    {
        double green = dcsGreenProduction[greenPowerItem->first];
        double time = round(simgrid::s4u::Engine::get_clock());
        
        // if we are at night, we conserve the previous gap
        if(getIdealPowerAtTime(time,greenPowerItem->first) > 0)
        {
            double theGap = green - getIdealPowerAtTime(time,greenPowerItem->first);
            gap[greenPowerItem->first] = theGap;
        }
        
        greenPower[greenPowerItem->first][time] = green;
    }    
    //XBT_INFO("green power updated");
}

void Manager::nemesisBehavior()
{
    
    
    while(delayedvms.size()>0)
    {           
        vms.push_back(delayedvms[0]);
        delayedvms.erase(delayedvms.begin());
    }    

    if(!delayedvms.empty())
    {
        for(VMRequest *vm : delayedvms)
        {
            vms.push_back(vm);
        }
        delayedvms.clear();

    }

        
    //XBT_INFO("TEM VM");
    double ctime = round(simgrid::s4u::Engine::get_clock());
    allocateVm();      
    migratePreallocations();    
    migrateRunningVM();      
    for(std::string dc : dcsName)
    {
        consolidateDC(dc);
    }

    
    deployPreallocations();
    startMigrations();
    shutDownEmptyHosts();  
    

    
}

/**
 * Preallocate the incoming VMs to servers. 
 * 
 */
void Manager::allocateVm()
{

    auto t1 = std::chrono::high_resolution_clock::now();        

    //XBT_INFO("Allocating vms");
    std::string destServerName;
    //First we sort the VMs by core    
    std::stable_sort(vms.begin(), vms.end(), sortVMByCPU);           

    //Then we traverse through the VMs    
    for(VMRequest *vm : vms)
    {           
        auto ini = std::chrono::high_resolution_clock::now(); 

        // first, get the set of of servers among all DCs, which have the minimum available space sufficient to receive the VM.
        std::vector<std::string> servers = getServersWithMinAvailableVolume(vm->getNbCores());

        auto end1 = std::chrono::high_resolution_clock::now();
        auto time1 = std::chrono::duration_cast<std::chrono::nanoseconds>( end1 - ini ).count();
        
        //std:://cout << "VM "<< vm->getName() << " getServersWithMinAvailableVolume " << time1 << endl;
        //std:://cout << " # servers:  " << servers.size() << endl;
        // if only one server meet this criteria then we've got a match
        if(servers.size()==1)
        {
            destServerName = *servers.begin();            
        }
        else
        {                                

            // if several hosts with this available volume exist, then we take the one with the minimum expected brown consumption cost.
            double minCost = std::numeric_limits<double>::max() ;
            for(std::string worker : servers)
            {          

                auto ini2 = std::chrono::high_resolution_clock::now();                                
                double cost = expectedBrownVmAdditionCost(worker, vm);

                if(cost < minCost)
                {

                    minCost = cost;
                    destServerName = worker;                    

                }

                auto end2 = std::chrono::high_resolution_clock::now();
                auto time2 = std::chrono::duration_cast<std::chrono::nanoseconds>( end2 - ini2 ).count();                                                                                
              //  std:://cout << "worker " << worker << " : " << time2 << endl;

            }
        }

        auto end2 = std::chrono::high_resolution_clock::now();
        auto time2 = std::chrono::duration_cast<std::chrono::milliseconds>( end2 - ini ).count();
        //   std:://cout << "VM "<< vm->getName() << " expectedbrownvm " << time2 << endl;
        //  finally, preallocate the VM, and update the power consumption accordingly.
        if(!destServerName.empty())
        {
            
            double power = powerConsumptions[*serversInformation[destServerName]->getDCName()];                                    
            powerConsumptions[*serversInformation[destServerName]->getDCName()] = power + vmAdditionCost(destServerName, vm);                   
            preallocations[vm]=destServerName;
            

        }
        else 
        {
            //XBT_INFO("VM could not be allocated at current time slot: %f, vm name %s",simgrid::s4u::Engine::get_clock,vm->getName().c_str())            ;;
            delayedvms.push_back(vm);
            //throw new Error("Could not find a host to deploy "+vm);            
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto time = std::chrono::duration_cast<std::chrono::nanoseconds>( end - ini ).count();
        //std:://cout << "VM "<< vm->getName() << " took " << time << endl;
        prepareForNemesis();
    }

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
    //   std:://cout << "Exec took " << duration << endl;


}


/**
 * Deploy the preallocated VMs on the hosts.
 */
void Manager::deployPreallocations() 
{    
    Message *message; 
    std::string dest;

    for(auto itemPreallocation = preallocations.begin();
            itemPreallocation!= preallocations.end();
            ++itemPreallocation )    
    {

        itemPreallocation->first->setDestHost(*serversInformation[itemPreallocation->second]->getHostName());
        serversInformation[itemPreallocation->second]->setAvaliableCores(
                serversInformation[itemPreallocation->second]->getAvailableCores() - itemPreallocation->first->getNbCores() );

        cout << round(simgrid::s4u::Engine::get_clock())<< ";" << itemPreallocation->first->getName() << ";" << itemPreallocation->second << endl;
        if(serversInformation[itemPreallocation->second]->serverIsOn())
        {                          
            //XBT_INFO("SERVER ON");
            dest = "worker_"+itemPreallocation->second;
            message =   new Message(MESSAGE_START_VM_EXECUTION, itemPreallocation->first) ;                            
        }
        else
        {
            //XBT_INFO("SERVER OFF");
            dest = *serversInformation[itemPreallocation->second]->getDCName();
            message = new Message(MESSAGE_TURN_HOST_ON, itemPreallocation->first) ;

            /*
                TODO:: validate ->>> DO I NEED TO DO THIS??
                    if(target.getStatus() == WorkerVMManager.OFF_STATUS){
					// TODO: we do this because auto-start does not work remove when fixed
					WorkerVMManager oldTarget = target;
					target = powerOnHost(target);
					
					for(VmDescription aVm : preallocations.keySet()){
						if(preallocations.get(aVm).equals(oldTarget)){
							preallocations.put(aVm, target);
						}
					}
					for(Migration mig : migrationsList){
						if(mig.getDestination().equals(oldTarget)){
							mig.setDestination(target);
						}
					}
				}
                */
        }

        MailBoxSingleton::GetInstance()->sendMessage(dest,message);
        
    }

}


/**
 * Compute the expected brown power cost of adding a given VM in a given server in a given DC considering the preallocations and migration previously made.
 * NB : This function can be used to compute the cost of a VM on the host it is already pre-allocated. 
 * @param dc The ID of the DC.
 * @param worker The server where the VM would be deployed.
 * @param vm The VM to deploy.
 * @return The expected brown power cost of adding the VM.
 */
double Manager::expectedBrownVmAdditionCost(const std::string& worker, VMRequest *vm)
{
    double cost = 0.0;
    std::string dc = *serversInformation[worker]->getDCName();
    double t = round(simgrid::s4u::Engine::get_clock());
    double P = powerConsumptions[dc];
    double tMax = t + vm ->getRemainingDuration() ;
    
  
    if(preallocations.find(vm)!=preallocations.end() && preallocations[vm].compare(worker) == 0)
    {                
        if(!optimizeFinancialCost)
        {
            double costa = expectedBrownPowerUntilSunset(dc, P,tMax);
            double costb = expectedBrownPowerUntilSunset(dc, (P - vmAdditionCost(worker, vm)),tMax);
            cost = costa - costb;
        }        
        else
        {
            double *Pow = new double[dcsName.size()];
            double *P_remove = new double[dcsName.size()];
            for(int j=0;j<dcsName.size();j++)
            {
                *(Pow+j) = powerConsumptions[dcsName[j]];
                if(dcsName[j].compare(dc) == 0)
                {
                    *(P_remove + j) = *(Pow +j) - vmAdditionCost(worker, vm);
                }else{
                    *(P_remove+j) = *(Pow +j);
                }
            }
            cost = expectedFinancialCostsUntilSunset(Pow, tMax,0) - expectedFinancialCostsUntilSunset(P_remove, tMax,0);
        }
        
    }
    else
    {                                              
        if(!optimizeFinancialCost)
        {
            double vmaddcost = vmAdditionCost(worker, vm);                        
            double expectedbcosta = expectedBrownPowerUntilSunset(dc, P + vmaddcost,tMax);                                    
            double expectedbcostb = expectedBrownPowerUntilSunset(dc, P,tMax);    
            cost = expectedbcosta - expectedbcostb;
        }
        else
        {
            double *Pow = new double[dcsName.size()];
            double *P_add = new double[dcsName.size()];
            for(int j=0;j<dcsName.size();j++){
                *(Pow+j)  = powerConsumptions[dcsName[j]];
                if(dcsName[j].compare(dc)==0){
                    *(P_add +j) = *(Pow + j) + vmAdditionCost(worker, vm);
                }else{
                    *(P_add +j) = *(Pow + j);
                }
            }
            double costa = expectedFinancialCostsUntilSunset(P_add, tMax, 0);
            double costb = expectedFinancialCostsUntilSunset(Pow, tMax,0);  
            cost =  costa - costb ;
        }
        
    }    
    return cost;
}


/**
 * Compute the power cost of adding a VM in a given server considering the preallocations and pre-migrations.
 * NB : This function can be used to compute the cost of a VM on the host it is already pre-allocated. 
 * @param worker The server where the VM would be deployed.
 * @param vm The VM.
 * @return The cost of adding a VM.
 */
double Manager::vmAdditionCost(const std::string& worker, VMRequest *vm)
{
    int nbCore = vm->getNbCores();
    // the cost of running the VM
    double cost = nbCore * p_core;
    
    bool preallocationAlreadyExist = false;
    if(preallocations.find(vm) != preallocations.end() && preallocations[vm].compare(worker) ==0   )
    {
        preallocationAlreadyExist = true;
        preallocations.erase(vm);
    }
    
    // we only add the cost of switching on an host if the host is off AND if we do not already have made some preallocations or migrations on it.
    if(isWorkerOffWithPreallocationsAndMigrations(worker))
    {
        // TODO : I remove p_on because its make no sense to consider temporary power (energy would be better)
        cost += p_idle - p_down;
    }
    
    if(preallocationAlreadyExist)
    {
        preallocations[vm]= worker;
    }
    
    return cost;

}


/**
 * Compute the average green power production of a given DC for the current period.
 * @param dc The ID of the DC.
 * @return the average green power production.
 */
double Manager::avg(const std::string& dc)
{

    std::map<double,double> greenPowHistory = greenPower[dc];
    double avg = 0;    

    if(greenPowHistory.size()!=0) 
    {

        double duration = 0;        
        double sum = 0;        
        double greenPowDuration;
         std::map<double,double>::iterator it2 = ++greenPowHistory.begin();
        std::map<double,double>::iterator it = greenPowHistory.begin();        
        std::map<double,double>::iterator end = --greenPowHistory.end();        

        while(it != end)
        {            
            greenPowDuration = it2->first - it->first;            
            sum+= it->second * greenPowDuration;
            duration +=greenPowDuration;
            ++it2;     
            ++it;       
        }
        
        greenPowDuration = round(simgrid::s4u::Engine::get_clock()) - greenPowHistory.rbegin()->first;
        double greenPow =  greenPowHistory.rbegin()->second;
        
        sum+= greenPow * greenPowDuration;
        duration +=greenPowDuration;
        
        avg = sum/duration;		
    }

    return avg;	

}

/**
 * Compute the standard deviation of the green power production of a given DC for the current period.
 * @param dc The ID of the DC.
 * @return The standard deviation.
 */
double Manager::standardDeviation(const std::string& dc)
{
		

    if(dcStdDevMap.find(dc)!=dcStdDevMap.end()) return dcStdDevMap[dc];
    std::map<double,double>& greenPowHistory = greenPower[dc];		
    double stdDev = 0;		
    if(greenPowHistory.size()!=0) 
    {							
        
        double average = avg(dc);
        double duration = 0.0;			
        double sumGreenPow = 0;

        //  for(std::map<double,double>::iterator it = std::next(idealGreenPow.begin()) ; it != idealGreenPow.end(); ++it){
        // we compute the sum of the squared deviations of the values in the history (i.e. the already received events) from the mean
        // as the values events are not always regular, we take account of the duration of each value.                                
        double greenPowDuration;
        std::map<double,double>::iterator it2 = ++greenPowHistory.begin();
        std::map<double,double>::iterator end =  --greenPowHistory.end();
        for(std::map<double,double>::iterator it = greenPowHistory.begin(); it != end; ++it)
        {
            greenPowDuration = it2->first - it->first;	
            sumGreenPow += pow( it->second -average , 2) * greenPowDuration;				
            duration +=greenPowDuration;
            ++it2;
        }			            
        
        greenPowDuration = round(simgrid::s4u::Engine::get_clock()) - greenPowHistory.rbegin()->first;;
        double greenPow = greenPowHistory.rbegin()->second;			
        sumGreenPow += pow( greenPow-average , 2) * greenPowDuration;			
        duration +=greenPowDuration;			
        double variance = sumGreenPow / duration;			
        stdDev = sqrt(variance);		

    }				
    dcStdDevMap[dc] = stdDev;
    return stdDev;
}


/**
 * Compute the times when the VM deployed (or preallocated, or migrating in) on a given DC will be shut down.
 * @param dc The ID of the DC.
 * @return The times where VM will be shut down in format {(time,nbCore)}*.
 */
void Manager::getVmExtinctionTimes(const std::string& dc)
{
    //std::map<double,std::vector<VMInterface*>> extinctionTimes;
    double extinctionTime;    
    ////cout << round(simgrid::s4u::Engine::get_clock()) << " no vmcache" << endl;
    dcVMExtinctionTimes[dc];
    std::vector<WorkerInformation*>::iterator servers_it = dcWorkersInfo[dc].begin();
    std::vector<WorkerInformation*>::iterator end = dcWorkersInfo[dc].end();        
    // first, we check the VM which are already deployed
    while(servers_it != end)    
    {   
        robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = (*servers_it)->getRunningVMS();
        if( runningVMSMap==nullptr || (*runningVMSMap).empty() ||(*runningVMSMap).size()==0)
        {
            ++servers_it;
            continue;
        }
        for(auto item = runningVMSMap->begin();
                item != runningVMSMap->end(); ++item)
        {
            extinctionTime = round(simgrid::s4u::Engine::get_clock() + item->second->getRemainingDuration());                                    
            // if the VM is migrating outside the DC, then its extinction time in the DC is at the end of the migration.
            for(MigrationPlan* mig : migrationsList)
            {                
                if( mig->getVM() == item->second && 
                (*(*servers_it)->getDCName()).compare(*serversInformation[mig->getHost()->get_name()]->getDCName()) !=0 )
                {
                    extinctionTime = mig->getTime() + computeMigrationTime(mig->getVM(), true, false);
                    break;
                }
            }        
            // TODO : if the VM is migrating inside the DC, maybe we need to take account of the CPU during the migration		            
            if(dcVMExtinctionTimes[dc].find(extinctionTime) ==dcVMExtinctionTimes[dc].end() )
            {
                dcVMExtinctionTimes[dc][extinctionTime];
            }
            dcVMExtinctionTimes[dc][extinctionTime].push_back(item->second);
        }
        ++servers_it;
    } 

    for(auto item = preallocations.begin(); item != preallocations.end();++item)
    {               
        if((*serversInformation[item->second]->getDCName()).compare(dc) == 0)
        {
            extinctionTime = round(simgrid::s4u::Engine::get_clock() + item->first->getRemainingDuration());
            dcVMExtinctionTimes[dc][extinctionTime].push_back(item->first);
        }
    }

    // finally, we consider the VMs migrating in the DC (WARNING : we make the hypothesis that the VM will shutdown AFTER the end of the migration).
    for(MigrationPlan* mig : migrationsList)
    {    
        DeployableVM* vm = mig->getVM();            
        if(
            vm->getWorkerActor()->getDCName().compare(dc)!=0 // the vm is not running in  the current DC
            &&
            (*serversInformation[mig->getHost()->get_name()]->getDCName()).compare(dc)==0 // the destination of the migrating vm is a server into the current dc
        )
        {
            double extinctionTime = round(simgrid::s4u::Engine::get_clock() + vm->getRemainingDuration());            
            
            if(dcVMExtinctionTimes[dc].find(extinctionTime) == dcVMExtinctionTimes[dc].end())
            {
                dcVMExtinctionTimes[dc][extinctionTime];
            }
            dcVMExtinctionTimes[dc][extinctionTime].push_back(vm);
        }
    }
		
}



/**
 * Compute the times where hosts will shut-down (in the absence of new incoming VM) in a given DC according to the preallocations and the migrations.
 * @param dc the ID of the DC.
 * @param vmExtinctionTimes the times when the VM deployed (or preallocated, or migrating in) on a given DC will be shut down.
 * @return The times where hosts will shut-down in the format {(time,nb_host_to_shut_down)}*
 */
void  Manager::getHostsExtinctionTimes(const std::string& dc,const std::map<double,std::vector<VMInterface*>>& vmExtinctionTimes)
{

    int nbAvailableCores;
  
    ////cout << round(simgrid::s4u::Engine::get_clock()) << " no hostcache" << endl;
    //dcHostsExtinctionTimes
    std::vector<WorkerInformation*>::iterator servers_it = dcWorkersInfo[dc].begin();
    std::vector<WorkerInformation*>::iterator end = dcWorkersInfo[dc].end();
    dcHostsExtinctionTimes[dc];
    
    // first, we check the VM which are already deployed
    while(servers_it != end)    
    {
        
        if((*servers_it)->serverIsOn() )
        {            
            const std::string& hostName = *(*servers_it)->getHostName();   
        
            
            ////cout << round(simgrid::s4u::Engine::get_clock()) << " host no allocate " << hostName << endl ;
            
            nbAvailableCores = nbAvailableCoresWithPreallocationsAndMigrations(hostName);                                
            for(auto itemVMExtinction =vmExtinctionTimes.begin(); itemVMExtinction != vmExtinctionTimes.end(); ++itemVMExtinction )
            {
                for(VMInterface* vm: itemVMExtinction->second)
                {
                    if( dynamic_cast<VMRequest*>(vm) != nullptr )
                    {
                        if(preallocations[static_cast<VMRequest*>(vm)].compare(hostName) == 0)
                        {
                            nbAvailableCores+=vm->getNbCores();
                        }
                    }
                    else 
                    {
                        bool isMigrating = false;
                        for(MigrationPlan* mig : migrationsList)
                        {
                            if(mig->getHost()->get_name().compare(*(*servers_it)->getHostName())==0 )
                            {
                                isMigrating = true;
                                break;
                            }
                        }
                        bool vmInWorker = false;
                        robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = (*servers_it)->getRunningVMS();
                        if( runningVMSMap!=nullptr && !(*runningVMSMap).empty() )
                        {
                            for(auto itemServerInfo = runningVMSMap->begin();
                                    itemServerInfo != runningVMSMap->end(); 
                                    ++itemServerInfo)
                            {
                                if(itemServerInfo->second == static_cast<DeployableVM*>(vm))   
                                {
                                    vmInWorker = true;
                                    break;
                                }
                            }
                        }

                        if(vmInWorker || isMigrating )   
                        {
                            nbAvailableCores += vm->getNbCores();

                        }

                    }
                }                   
 
                if(nbAvailableCores == simgrid::s4u::Host::by_name(hostName)->get_core_count())
                {
        
                    int nbHost = 1;
                    if(dcHostsExtinctionTimes[dc].find(itemVMExtinction->first)!=dcHostsExtinctionTimes[dc].end())
                    {
                        nbHost += dcHostsExtinctionTimes[dc][itemVMExtinction->first];
                    }
                    dcHostsExtinctionTimes[dc][itemVMExtinction->first]= nbHost;
                    break;
                }

            } 

        }
        ++servers_it;        
    }


}



/**
 * Compute the expected brown power consumption of a given DC until a given date (or until sunset if sunset is met before).
 * We consider that the sunset occurs when the ideal green power production is equals to zero.
 * Thus, the ideal green power production trajectory should be compliant with this hypothesis.
 * @param dc The ID of the DC.
 * @param P The power consumption of the DC.
 * @param t_max The date until when the brown power consumption should be computed.
 * @return The expected brown power consumption.
 */
double Manager::expectedBrownPowerUntilSunset(const std::string& dc,double P,const double& t_max)
{

    auto ini_standardDev = std::chrono::high_resolution_clock::now();
    double standardDev = standardDeviation(dc);
    auto end_standardDev = std::chrono::high_resolution_clock::now();
    auto time_standardDev = std::chrono::duration_cast<std::chrono::nanoseconds>( end_standardDev - ini_standardDev ).count();
                                
    ////cout << "_standardDev " << time_standardDev << endl;
    double time = round(simgrid::s4u::Engine::get_clock());

    auto ini_idealPow = std::chrono::high_resolution_clock::now();

    double idealPow = getIdealPowerAtTime(time, dc);	

    auto end_idealPow = std::chrono::high_resolution_clock::now();
    auto time_idealPow = std::chrono::duration_cast<std::chrono::nanoseconds>( end_idealPow - ini_idealPow ).count();
   // //cout << "_idealPow " << time_idealPow << endl;


    double totalBrown = 0;
    auto inivmExtinctionTimes = std::chrono::high_resolution_clock::now();

    
    if(dcVMExtinctionTimes.find(dc)== dcVMExtinctionTimes.end() )
    {
    //    //cout << round(simgrid::s4u::Engine::get_clock()) << " vmcache" << endl;
        getVmExtinctionTimes(dc);
    }    
    map<double,std::vector<VMInterface*>>& vmExtinctionTimes = dcVMExtinctionTimes[dc];// = getVmExtinctionTimes(dc);
    auto endvmExtinctionTimes = std::chrono::high_resolution_clock::now();
    auto timevmExtinctionTimes = std::chrono::duration_cast<std::chrono::nanoseconds>( endvmExtinctionTimes - inivmExtinctionTimes ).count();
    ////cout << " " << timevmExtinctionTimes << endl;
    auto inihostExtinctionTimes = std::chrono::high_resolution_clock::now();
    if(dcHostsExtinctionTimes.find(dc)== dcHostsExtinctionTimes.end())
    {
      //  //cout << round(simgrid::s4u::Engine::get_clock()) << " hostcache" << endl;
        getHostsExtinctionTimes(dc, vmExtinctionTimes);
    }      
    map<double,int>& hostExtinctionTimes= dcHostsExtinctionTimes[dc];

    auto endhostExtinctionTimes = std::chrono::high_resolution_clock::now();
    auto timehostExtinctionTimes = std::chrono::duration_cast<std::chrono::nanoseconds>( endhostExtinctionTimes - inihostExtinctionTimes ).count();
    ////cout << "hostExtinctionTimes " <<  << endl;
    ////cout<< timevmExtinctionTimes  <<";"<< timehostExtinctionTimes << endl;    
    // if we start at night, then we compute until the next sunset


    bool isNight = (idealPow == 0);
    auto iniLoop = std::chrono::high_resolution_clock::now();
    map<double,int>::iterator it_host = hostExtinctionTimes.begin();
    map<double,std::vector<VMInterface*>>::iterator it_vm = vmExtinctionTimes.begin();
    
    int niterations =0;

    while( (idealPow > 0 || isNight) && time <= t_max)
    {        

        niterations++;
        double tempP =0;        
        auto iniLoopVM = std::chrono::high_resolution_clock::now();
        
        while(it_vm != vmExtinctionTimes.end() && it_vm->first < time + timeslot)
        {
            for(VMInterface* vm : it_vm->second)
            {
                P -=  vm->getNbCores() * p_core;
            }
            ++it_vm;
        }        

        auto endLoopVM = std::chrono::high_resolution_clock::now();
        auto timeLoopVM = std::chrono::duration_cast<std::chrono::nanoseconds>( endLoopVM - iniLoopVM ).count();
        //   //cout << "LoopVM " << timeLoopVM << endl;

        auto iniLoopHost = std::chrono::high_resolution_clock::now();

        while(it_host != hostExtinctionTimes.end() && it_host->first < time + timeslot)
        {            
           int nbHostOff = it_host->second;
            P += nbHostOff * (p_down - p_idle);
            tempP += nbHostOff *p_off;            
            ++it_host;

        }
       

        auto endLoopHost = std::chrono::high_resolution_clock::now();
        auto timeLoopHost = std::chrono::duration_cast<std::chrono::nanoseconds>( endLoopHost - iniLoopHost ).count();
        //  //cout << "LoopHost " << timeLoopHost << endl;
        auto initotalBrown = std::chrono::high_resolution_clock::now();

        totalBrown += expectedBrownPower(dc, P + tempP, time, standardDev);

        auto endtotalBrown = std::chrono::high_resolution_clock::now();
        auto timetotalBrown = std::chrono::duration_cast<std::chrono::nanoseconds>( endtotalBrown - initotalBrown ).count();
        //   //cout << "totalBrown " << timetotalBrown << endl;

        time += timeslot;
        auto iniidealPow = std::chrono::high_resolution_clock::now();
        idealPow = getIdealPowerAtTime(time,dc);      
        auto endidealPow = std::chrono::high_resolution_clock::now();
        auto timeidealPow = std::chrono::duration_cast<std::chrono::nanoseconds>( endidealPow - iniidealPow ).count();
        //  //cout << "idealPow " << timeidealPow << endl;

        if(isNight && idealPow > 0)
        {
            isNight = false;
        }        

    }

    auto endLoop = std::chrono::high_resolution_clock::now();
    auto timeLoop = std::chrono::duration_cast<std::chrono::nanoseconds>( endLoop - iniLoop ).count();    
    ////cout << round(simgrid::s4u::Engine::get_clock())  << ";" <<   time_standardDev << ";" << time_idealPow << ";" << timevmExtinctionTimes << ";" << timehostExtinctionTimes << ";" << timeLoop << endl ;
    return totalBrown;
}

/**
 * Compute the expected brown power consumption of a given DC for a given time-slot.
 * @param dc The ID of the DC.
 * @param t The date of the time-slot
 * @param P The power consumption of the host in the DC.
 * @param standardDev The standard deviation of the green power production. 
 * @return The expected brown power consumption of a given DC.
 */

double Manager::expectedBrownPower(const std::string& dc,const double& P,const double& t,const double& standardDev)
{

    double greenPow = expectedGreenPow(dc,t);    
    double ret = P - greenPow;    

    if(standardDev != 0)
    {
        ret *= (Phi(P,greenPow,standardDev) - Phi(0,greenPow,standardDev))/(1.0-Phi(0,greenPow,standardDev));
        ret -= pow(standardDev,2) * (phi(0,greenPow,standardDev)-phi(P,greenPow,standardDev)) / (1.0 - Phi(0,greenPow,standardDev));
    }

    if(isnan(ret)) 
    {
        std::string error ="expected brown power for "+dc+" = "+std::to_string(ret)+" greenPow = "+std::to_string(greenPow)+" standardDev = "+std::to_string(standardDev)+" Phi(P) = "+std::to_string(Phi(P,greenPow,standardDev))+" Phi(0,) = "+std::to_string(Phi(0,greenPow,standardDev))+" phi(P) = "+std::to_string(phi(P,greenPow,standardDev))+" phi(0,) = "+std::to_string(phi(0,greenPow,standardDev));
        //XBT_INFO("%s",error.c_str());
    }

    return ret;

}

/**
 * Compute the probability density function function of the green power production.
 * @param x green power input of the function.
 * @param greenPow The expected green power production.
 * @param standardDev The standard deviation of the green power production.
 * @return the value of phi(x).
 */
double Manager::phi(const double& x,const double& greenPow,const double& standardDev)
{
    double powValue = -0.5 * pow( (x - greenPow)/standardDev , 2.0);    
    return (1.0/(standardDev * sqrt(2.0 * M_PI))) * exp(powValue );
}



/**
 * Compute the cumulative distribution function of the green power production.
 * @param x
 * @param greenPow The expected green power production.
 * @param standardDev The standard deviation of the green power production.
 * @return the value of Phi(x).
 */
double Manager::Phi(const double& x,const  double& greenPow,const double& standardDev)
{    
    double erfValue = erf((x-greenPow)/(standardDev * sqrt(2)));   
    return 0.5 * (1.0 + erfValue);
}

	/**
	 * Compute an approximation to the erf function.
	 * SOURCE : http://introcs.cs.princeton.edu/java/21function/ErrorFunction.java.html
	 * fractional error in math formula less than 1.2 * 10 ^ -7.
	 *  although subject to catastrophic cancellation when z in very close to 0
	 *  from Chebyshev fitting formula for erf(z) from Numerical Recipes, 6.2
	 * @param z
	 * @return erf(z).
	 */
    double  Manager::erf(const double& z) 
    {
        double t = 1.0 / (1.0 + 0.5 * abs(z));

        // use Horner's method
        double ans = 1 - t * exp( -z*z   -   1.26551223 +
                                            t * ( 1.00002368 +
                                            t * ( 0.37409196 + 
                                            t * ( 0.09678418 + 
                                            t * (-0.18628806 + 
                                            t * ( 0.27886807 + 
                                            t * (-1.13520398 + 
                                            t * ( 1.48851587 + 
                                            t * (-0.82215223 + 
                                            t * ( 0.17087277))))))))));
        if (z >= 0) return  ans;
        else        return -ans;
    }





/**
 * Compute the expected green power production of a given DC for a given time-slot.
 * @param dc the ID of the DC.
 * @param t the date of the time-slot.
 * @return The expected green power production of a given DC for a given time-slot.
 */
double Manager::expectedGreenPow(const std::string& dc,const double& t)
{
    double idealPow = getIdealPowerAtTime(t,dc);
    double estimatedGreenPow = max(0.0, idealPow + gap[dc] );
    return estimatedGreenPow;
}

/**
 * Return the ideal power production at a given time scaled according to the PV equipment of a given DC.
 * @param time The time of the production.
 * @param dc The ID of the DC.
 * @return the power production.
 */
double Manager::getIdealPowerAtTime(double time,const std::string& dc)
{
 
 
    if(dcsIdealPow.find(time)!=dcsIdealPow.end() && dcsIdealPow[time].find(dc)!=dcsIdealPow[time].end() ) return dcsIdealPow[time][dc];
    // convert in the time since the debut of the day
    int nbDay = (int)(time/(24.0*60.0*60.0));
    time -= (nbDay * (24.0*60.0*60.0));    
    double idealValue = 0;    
    double lowerBound = idealGreenPow.begin()->first;
    double upperBound = 0.0;
    std::map<double,double>::iterator begin = ++idealGreenPow.begin();
    std::map<double,double>::iterator end = --idealGreenPow.end();

    for(std::map<double,double>::iterator it = begin  ; it != end; ++it)
    {
        // for each time interval
        upperBound = it->first;        
        // if the current value is in this interval, then we make a linear interpolation to find the ideal value corresponding to this time
        if((time>=lowerBound)&&(time<=upperBound))
        {            
            double lowerBoundValue = idealGreenPow[lowerBound];
            double upperBoundValue = idealGreenPow[upperBound];                            
            idealValue = lowerBoundValue + (time-lowerBound) * (upperBoundValue-lowerBoundValue)/(upperBound-lowerBound);        
        }
        
        else if (upperBound > time ) break;

        lowerBound = upperBound;

    }  

    // finally we scale according to the number of PV in the DC.
    
    idealValue = idealValue * nbPv[dc];
    dcsIdealPow[time][dc] = idealValue ;
    return idealValue ;
}

/**
 * Determine if a given server is OFF considering the current status of the host AND the preallocations and pre-migrations previously made.
 * @param worker The worker.
 * @return true if the worker is OFF.
 */
bool Manager::isWorkerOffWithPreallocationsAndMigrations(const std::string& worker)
{
    bool isMigration = false;
    bool workerInPreallocations = false;
    
    for(MigrationPlan* mig : migrationsList)
    {
        if(mig->getHost()->get_name().compare(worker) == 0)
        {
            isMigration = true;
            break;
        }
    }          

    for(auto prealloc = preallocations.begin(); prealloc != preallocations.end(); ++prealloc)
    {
        if(prealloc->second.compare(worker)==0)
        {
            workerInPreallocations = true;
            break;
        }
    }
    return !serversInformation[worker]->serverIsOn() && ! isMigration  && !workerInPreallocations;// && worker.getStatus()==WorkerVMManager.OFF_STATUS && !preallocations.containsValue(worker) && !isMigration;    
}




std::vector<std::string> Manager::getServersWithMinAvailableVolume(const int& vCPU)
{

    auto ini = std::chrono::high_resolution_clock::now(); 

    std::vector<std::string> workers = getSortedServers(true);    
    auto end1 = std::chrono::high_resolution_clock::now();
    auto time1 = std::chrono::duration_cast<std::chrono::nanoseconds>( end1 - ini ).count();
    //std:://cout << "getsortedservers  " << time1 << endl;
    auto ini2 = std::chrono::high_resolution_clock::now(); 
    while(workers.size()>0 && nbAvailableCoresWithPreallocationsAndMigrations(workers[0]) < vCPU )
    {
        workers.erase(workers.begin());
    }
    auto end2 = std::chrono::high_resolution_clock::now();
    auto time2 = std::chrono::duration_cast<std::chrono::nanoseconds>( end2 - ini2 ).count();
    //std:://cout << "first while  " << time2 << endl;

    auto ini3 = std::chrono::high_resolution_clock::now(); 
    if(workers.size()!=0) 
    {
        int minSpace = nbAvailableCoresWithPreallocationsAndMigrations(workers[0]);
        while(workers.size()!=0 && nbAvailableCoresWithPreallocationsAndMigrations(workers[workers.size()-1]) > minSpace)
        {
            workers.pop_back();
        }
    }
    auto end3 = std::chrono::high_resolution_clock::now();
    auto time3 = std::chrono::duration_cast<std::chrono::nanoseconds>( end3 - ini3 ).count();
    //std:://cout << "2nd while  " << time3 << endl;
    return workers;
}



/**
 * Compute the set of of servers on a given DC which have the minimum available space sufficient to receive a given volume.
 * The method takes account of the preallocations previously made.
 * @param dc The ID of the DC.
 * @param mustBeOn Specify if we only want servers ON.
 * @param vCpu The volume the servers must be able to receive.
 * @return The set of of servers.
 */
std::vector<std::string> Manager::getServersWithMinAvailableVolume(const std::string& dc,const bool& mustBeOn,const int& vCpu)
{

    std::vector<std::string> workers = getServersWithMinAvailableVolume(vCpu);
    std::vector<std::string> dcWorkers;


    for(std::string worker : workers)
    {   
        if((*serversInformation[worker]->getDCName()).compare(dc) == 0)
        {
            if(!mustBeOn || !isWorkerOffWithPreallocationsAndMigrations(worker))
            {
                dcWorkers.push_back(worker);    
            }            

        }

    }

    // Then, we sort the workers by increasing size of available number of vCPU.
    std::stable_sort(dcWorkers.begin(),dcWorkers.end(),
    
    [this](std::string  first, std::string  second) -> bool {
        int nCoresFirst =nbAvailableCoresWithPreallocationsAndMigrations(first);
        int nCoresSecond =nbAvailableCoresWithPreallocationsAndMigrations(second);
        return  nCoresFirst< nCoresSecond;
    });

    return dcWorkers;
}


/**
 * Sort the servers among all data-centers by size of available number of vCPU.
 * This method takes account of the preallocations previously made when sorting/filtering the servers.
 * @param increasing The type of sort (increasing or decrasing)
 * @return The list of not full servers among all data-centers sorted by increasing size of available number of vCPU.
 */

std::vector<std::string> Manager::getSortedServers(const bool& increasing)
{
    std::vector<std::string> result;    
    std::vector<std::pair<std::string,int>> servers_cores;
    //get all dcs servers
    for(std::string dc : dcsName )
    {
        //result.
        for(std::string server : dcServers[dc])
        {         
            servers_cores.push_back(make_pair(server,nbAvailableCoresWithPreallocationsAndMigrations(server)));
        }
    }

    if(increasing)
    {
        // Then, we sort the workers by increasing size of available number of vCPU.
        std::stable_sort(servers_cores.begin(),servers_cores.end(),
        
        /*
            @param this: represents the current class, so I can acess this class method from the lambda function
        */
        [this](std::pair<std::string,int> first, std::pair<std::string,int> second) -> bool 
        {
                                                
            return  first.second< second.second;
            
        });
    }
    else
    {
        // Then, we sort the workers by increasing size of available number of vCPU.
        std::stable_sort(servers_cores.begin(),servers_cores.end(),
        
        /*
            @param this: represents the current class, so I can acess this class method from the lambda function
        */
        [this](std::pair<std::string,int> first, std::pair<std::string,int> second) -> bool 
        {
                                                
            return  first.second > second.second;
            
        });
            
        
    }

    for(auto it =servers_cores.begin();it != servers_cores.end();++it )
    {
        result.push_back(it->first);
    }
 	

    return result;
}



void Manager::updateConsumptionsHistoric()
{
    
    double time = round(simgrid::s4u::Engine::get_clock());
    MailBoxSingleton::GetInstance()->sendMessage("grid", new Message(MESSAGE_UPDATE_ENERGY));    

    //Fill historic data
    for(std::string dc :  dcsName )
    {
        simgrid::s4u::this_actor::yield();
        MailBoxSingleton::GetInstance()->sendMessage(dc,new Message(MESSAGE_GET_ENERGY) );
        Message *message = MailBoxSingleton::GetInstance()->getMessage("energy_info"+dc);
        if(message != nullptr)
        {
            EnergyInformation *info = static_cast<EnergyInformation*>(message->data);
            consumptions_historic[dc].insert(make_pair(time,info->getCumulativeEnergyConsumption())) ;
        }
    }

}

int Manager::nbAvailableCoresWithPreallocationsAndMigrations(const std::string& worker)
{
    if(serversInformation.find(worker)==serversInformation.end()) return 0;            
    return nbAvailableCoresWithPreallocationsAndMigrations(serversInformation[worker]);
}


int Manager::nbAvailableCoresWithPreallocationsAndMigrations(WorkerInformation* worker)
{   

    int nbCore = worker->getAvailableCores();
    //Remove the cores from the preallocated vms
    for(auto item = preallocations.begin(); item != preallocations.end(); ++item )
    {
        if(item->second .compare(*worker->getHostName()) ==0)
        {
            nbCore -= item->first->getNbCores();            
            // cannnot decrease it anymore, so we stop the search
            if(nbCore == 0 ) break;
        }
    }
    
    for(MigrationPlan* mig : migrationsList)
    {
        if(mig->getHost()->get_name().compare(*worker->getHostName()) == 0)
        {
            nbCore -= mig->getVM()->getNbCores();
        }
    }


    return nbCore;
}

void Manager::loadIdealPowerTrajectory(const std::string& idealGreenPowPath)
{
    ifstream input;
    std::string  nextLine;
    std::vector<std::string> Datas;				
    try
    {
        input =  ifstream(idealGreenPowPath);                
        while(getline(input,nextLine))
        {        
            boost::algorithm::split(Datas, nextLine, boost::is_any_of(";"));
            idealGreenPow[std::stod(Datas[0])] = std::stod(Datas[1]);        
        }        
        input.close();
    }
    catch (const std::exception& e) 
    {            
        //cout << "Error  at MANAGER loadidealpowertrajectory: " << e.what() << endl;
    }	
}

void Manager::updateState()
{
    migrationsList.clear();
    preallocations.clear();
    powerConsumptions.clear();    
    for(std::string dc : dcsName)
    {
        powerConsumptions[dc]=  powerConsumption(dc);
    }
}

/**
 * Compute the power consumption of a given DC. 
 * @param dc The ID of the DC.
 * @return The power consumption of the DC.
 */
double Manager::powerConsumption(const std::string& dc)
{
    int nbHostOn = 0;
    int nbHostOff = 0;
    int nbCore = 0;
    for(std::string server : dcServers[dc])
    {
        if(serversInformation[server]->serverIsOn())
        {
            nbHostOn++;
            nbCore +=  
                simgrid::s4u::Host::by_name( *serversInformation[server]->getHostName() )->get_core_count() -  
                serversInformation[server]->getAvailableCores();

        }
        else
        {
            nbHostOff++;
        }

        /*if(dc.compare("toulouse.grid5000.fr") == 0)
        {
            cout << round(simgrid::s4u::Engine::get_clock()) <<";" << *serversInformation[server]->getHostName() << ";"<< nbCore<< endl;
            robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = serversInformation[server]->getRunningVMS();
            if( runningVMSMap!=nullptr && !(*runningVMSMap).empty() )
            {
                for(auto it = runningVMSMap->begin(); it != runningVMSMap->end(); ++it){
                    cout << it->second->getName() << endl;
                }
            }
        }*/
    }        
    return (nbHostOn * p_idle) + (nbHostOff * p_down) + (nbCore * p_core);
}

void Manager::performSchedule()
{     
    auto ini = std::chrono::high_resolution_clock::now();
   
    updateState();
    updateConsumptionsHistoric();
    prepareForNemesis();
    nemesisBehavior();
    updateGreenPow();
    vms.clear();
    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>( end - ini ).count();

   ////cout << round(simgrid::s4u::Engine::get_clock()) <<";" << time << endl;
}

void Manager::shutDownHosts()
{
  //  //XBT_INFO("SHUTTING DOWN HOSTSSS");
    for(simgrid::s4u::Host *host : simgrid::s4u::Engine::get_instance()->get_all_hosts())
    {
        if(host->get_name()!=simgrid::s4u::this_actor::get_host()->get_name())
        {
        //    //XBT_INFO("TURNING OFF HOST %s",host->get_cname());
            host->set_pstate(1);
            host->turn_off();
        }
    }
}



void Manager::printPreAllocations()
{

    for(auto itemPreallocation = preallocations.begin();
            itemPreallocation!= preallocations.end();
            ++itemPreallocation )    
    {
  //      //cout << itemPreallocation->first->getName() << ";" << itemPreallocation->second << endl;
    }

}


/**
 * "Migrate" (i.e. change) the preallocations previously made based on the Estimated Remaining Green Energy until sunset.
 */
void Manager::migratePreallocations()
{                        
    int nbChangeDone = 0;
    //  auto ini_sortdc = std::chrono::high_resolution_clock::now();
    std::vector<std::pair<std::string,double>> sortedDc = sorteDcByERGE();
    //auto end_sortdc = std::chrono::high_resolution_clock::now();
    //    auto time_sortdc = std::chrono::duration_cast<std::chrono::nanoseconds>( end - ini ).count();
    // //cout << round(simgrid::s4u::Engine::get_clock()) <<";" << time << endl;
    unordered_map<std::string, std::vector<VMRequest*>> sortedVms;     
    
    for( std::string dc  : dcsName )
    {
        std::vector<VMRequest*> vmList;

        for(auto preallocIterator = preallocations.begin(); preallocIterator!=preallocations.end();++preallocIterator)
        {
            //if the server that the vm was allocated is the same dc as "dc"
            if(  (*serversInformation[preallocIterator->second]->getDCName()).compare(dc) == 0 )
            {                
                vmList.push_back(preallocIterator->first);
            }

        }

        std::stable_sort(vmList.begin(),vmList.end(),sortVMByCPU);
        sortedVms[dc]=vmList;

    }

    // finally, we perform the migrations
    for(int idSending=0;idSending<sortedDc.size()-1;idSending++)
    {
        int idReceiving = sortedDc.size() - 1;
        std::string dcSending = sortedDc[idSending].first;
        std::string dcReceiving = sortedDc[idReceiving].first;;
        prepareForNemesis();

        while(idSending < idReceiving && sortedVms.find(dcSending) != sortedVms.end() && !sortedVms[dcSending].empty() )
        {


            for(int i =0; i< sortedVms[dcSending].size();i++)
            {
                VMRequest *vm =sortedVms[dcSending][i];
                std::vector<std::string> hostsOn = getServersWithMinAvailableVolume(dcReceiving, false, vm->getNbCores());

                if(!hostsOn.empty())
                {

                    // NB: as all the host are equivalent (i.e. same DC, same available volume) we take the first host of the list which is ON (if no host are ON,then we take the first host off).
                    std::string sourceWorker = preallocations[vm];
                    std::string targetWorker = hostsOn[0];
                                            
                    double Psending = powerConsumptions[dcSending];
                    double Preceiving = powerConsumptions[dcReceiving];
                    double tMax = round(simgrid::s4u::Engine::get_clock()) + vm->getRemainingDuration();
                                
                    double withoutMigrationCost = 0;
                    double withMigrationCost = 0;                    
                    if(!optimizeFinancialCost)
                    {
                        withoutMigrationCost = expectedBrownPowerUntilSunset(dcSending, Psending, tMax) + expectedBrownPowerUntilSunset(dcReceiving, Preceiving, tMax);
                        double withoutMig = expectedBrownPowerUntilSunset(dcSending, Psending - vmAdditionCost(sourceWorker, vm), tMax);
                        double withMig  =expectedBrownPowerUntilSunset(dcReceiving, Preceiving + vmAdditionCost(targetWorker, vm), tMax);
                        withMigrationCost =  withoutMig + withMig;
                    }
                    else
                    {
                            
                            double *P = new double[sortedDc.size()];
							double *P_migration = new double[sortedDc.size()];

                            int j=0;
							for(std::string dc : dcsName)
                            {
								*(P+j) = powerConsumptions[dc];
								if(dc.compare(dcSending) == 0)
                                {
									*(P_migration+j) = *(P+j) - vmAdditionCost(sourceWorker, vm);
								}else if(dc.compare(dcReceiving) == 0)
                                {
									*(P_migration+j) = *(P+j) + vmAdditionCost(targetWorker, vm);
								}else
                                {
									*(P_migration+j) = *(P+j);
								}
                                j++;
							}
							
							withoutMigrationCost = expectedFinancialCostsUntilSunset(P, tMax, 0);
							withMigrationCost = expectedFinancialCostsUntilSunset(P_migration, tMax, 0);

                    }
                    
                                                                        
                    if(withMigrationCost < withoutMigrationCost)
                    {
                        // we change the pre-allocation, remove the VM and update the power consumptions.
                        powerConsumptions[dcSending] = powerConsumptions[dcSending] - vmAdditionCost(sourceWorker, vm);
                        powerConsumptions[dcReceiving] = powerConsumptions[dcReceiving] + vmAdditionCost(targetWorker, vm);
                        
                        preallocations[vm] = targetWorker;                        
                        sortedVms[dcSending].erase( sortedVms[dcSending].begin() + i );
                        i--;
                        nbChangeDone++;
                        prepareForNemesis();
                    }


                }
            }
            
            idReceiving--;

        }


    }


}


std::vector<std::pair<std::string, double>> Manager::sorteDcByERGE()
{
    
    std::vector<std::pair<std::string, double>> result;
    unordered_map<std::string,double> dcList;

    for(auto powerConsumptionIterator = powerConsumptions.begin(); powerConsumptionIterator!= powerConsumptions.end();++powerConsumptionIterator)
    {        

        result.push_back(make_pair(powerConsumptionIterator->first,
            expectedRemainingGreenPow(powerConsumptionIterator->first,powerConsumptionIterator->second)));

    }

    std::stable_sort(result.begin(),result.end(),    
    [this](std::pair<std::string, double> firstItem, std::pair<std::string, double>  secondItem) -> bool {                          
        return  firstItem.second < secondItem.second;
    });

    return result;
}

/**
 * Compute the expected remaining green power until sunset for a given DC.
 * We consider that the sunset occurs when the ideal green power production is equals to zero.
 * Thus, the ideal green power production trajectory should be compliant with this hypothesis.
 * @param dc The ID of the DC.
 * @param P The power consumption of the DC.
 * @return the expected remaining green power until sunset.
 */
double Manager::expectedRemainingGreenPow(const std::string& dc,double P)
{
    double stdDev = standardDeviation(dc);
    double time = round(simgrid::s4u::Engine::get_clock());
    double idealPow = getIdealPowerAtTime(time, dc);
    double remainingGreenPow = 0;

    if(dcVMExtinctionTimes.find(dc)== dcVMExtinctionTimes.end() )
    {
        getVmExtinctionTimes(dc);
    }    
    map<double,std::vector<VMInterface*>>& vmExtinctionTimes = dcVMExtinctionTimes[dc];

    if(dcHostsExtinctionTimes.find(dc)== dcHostsExtinctionTimes.end())
    {
        getHostsExtinctionTimes(dc, vmExtinctionTimes);
    }      
    map<double,int>& hostExtinctionTimes= dcHostsExtinctionTimes[dc];
    // if we start at night, then we compute until the next sunset
    bool isNight = (idealPow == 0);
    while( idealPow > 0 || isNight)
    {
        double tempP = 0;
        // TODO : we do a coarse discretization here. find a better way
        while(vmExtinctionTimes.size()>0 && vmExtinctionTimes.begin()->first < time + timeslot)
        {            
            for(VMInterface* vm : vmExtinctionTimes.begin()->second)
            {
                P -=  vm->getNbCores() * p_core;
            }
            vmExtinctionTimes.erase(vmExtinctionTimes.begin());
        }        
        while(hostExtinctionTimes.size()>0 && hostExtinctionTimes.begin()->first < time + timeslot)
        {
            int nbHostOff = hostExtinctionTimes.begin()->second;
            P += nbHostOff * (p_down - p_idle);
            tempP +=nbHostOff * p_off;
            hostExtinctionTimes.erase(hostExtinctionTimes.begin());
        }        
        if(idealPow > 0)
        {
            remainingGreenPow += expectedRemainingGreenPow(dc,P + tempP,stdDev,time);
        }
        time += timeslot;
        idealPow = getIdealPowerAtTime(time,dc);        
        if(isNight && idealPow > 0)
        {
            isNight = false;
        }        
    }
    return remainingGreenPow;
}

/**
 * Compute the expected remaining green power of a given DC for a given time-slot.
 * @param dc The ID of the DC.
 * @param P The power consumption of the DC.
 * @param stdDev The standard deviation of the DC green power production.
 * @param t the time-slot
 * @return The expected remaining green power.
 */
double Manager::expectedRemainingGreenPow(const std::string& dc,const  double& P,const  double& stdDev,const  double& t)
{
		double green = expectedGreenPow(dc,t);
		double remainingGreen = green - P;
		
		if(stdDev!=0)
        {
			remainingGreen *= (1 - Phi(P,green,stdDev)) / (1 - Phi(0,green,stdDev));
			remainingGreen += pow(stdDev,2) * phi(P,green,stdDev) / (1 - Phi(0,green,stdDev));
		}		
		return remainingGreen;
}

/**
 * Schedule migrations of running VM.
 * The algorithm takes account of the preallocations previously made.
 */
void Manager::migrateRunningVM()
{   
    prepareForNemesis();
    std::vector<std::pair<std::string,double>> sortedDc = sorteDcByERGE();
    prepareForNemesis();
    unordered_map<std::string,std::vector<DeployableVM*>> listToMigrate = buildListOfVmToMigrate();

    int idSending = 0;
    int idReceiving = sortedDc.size()-1;
    std::string dcSending = sortedDc[idSending].first;
    std::string dcReceiving = sortedDc[idReceiving].first;
    
    double tmin = 0, tmax = 0;
    double *times = new double[2];        
    while(idSending < idReceiving)
    {                        
        std::vector<DeployableVM*>  listToMigrateSending = listToMigrate[dcSending];
        // sort the list of VM to send by decreasing volume.
        
        std::stable_sort(listToMigrateSending.begin(),listToMigrateSending.end(),    
        [this](DeployableVM* firstItem, DeployableVM*  secondItem) -> bool {                          
            if(firstItem->getNbCores() == secondItem->getNbCores())
            {
                return  firstItem->getName().compare(secondItem->getName()) <0;    
            }
            return  firstItem->getNbCores() > secondItem->getNbCores();
        });

        
        
        
        times = planVmMigrationsForDCs(dcSending,dcReceiving,listToMigrateSending,tmin,tmax,times);
        tmin= *times;
        tmax= *(times+1);
        
        // if some VM remains, then we change of DC_receiving.
        if(!listToMigrateSending.empty())
        {
            
            idReceiving--;                    
            dcReceiving = sortedDc[idReceiving].first;            
            times = planVmMigrationsForDCs(dcSending,dcReceiving,listToMigrateSending,tmin,tmax,times);
            tmin=times[0];
            tmax=times[1];
            
        }
        
        idSending++;
        dcSending = sortedDc[idSending].first;
    }	    
}


/**
 * Compute for each DC the list of running VM which should be migrated.
 * It takes account of the preallocations previously made.
 * @return the the list for each DC of running VM which should be migrated.
 */
unordered_map<std::string,std::vector<DeployableVM*>>  Manager::buildListOfVmToMigrate()
{    
    unordered_map<std::string,std::vector<DeployableVM*>> sortedVM;   
    for(std::string dc : dcsName)    
    {

        std::vector<WorkerInformation*>::iterator servers_it = dcWorkersInfo[dc].begin();
        std::vector<WorkerInformation*>::iterator end = dcWorkersInfo[dc].end();
        std::vector<std::pair<DeployableVM*,double>> vmList;
        // first, we check the VM which are already deployed
        while(servers_it != end)    
        {    
            robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = (*servers_it)->getRunningVMS();
            if( runningVMSMap==nullptr || (*runningVMSMap).empty() ||(*runningVMSMap).size()==0)
            {
                ++servers_it;
                continue;
            }
            for(auto item = runningVMSMap->begin(); item != runningVMSMap->end(); ++item)
            {
                if(!item->second->isVMMigrating())
                {
                    vmList.push_back(make_pair(item->second,round(item->second->getRemainingDuration())));
                }
            }
            ++servers_it;
        }           

        int x =0;
        x++;
        std::stable_sort(vmList.begin(),vmList.end(), [this]( std::pair<DeployableVM*,double> item1, std::pair<DeployableVM*,double> item2)->bool
        {
            if(item1.second == item2.second) return item1.first->getName().compare(item2.first->getName()) < 0;

            return  item1.second > item2.second;
        });


        std::vector<DeployableVM*> listToMigrate;
        double timeToMigrate = 0.0;
        for(std::pair<DeployableVM*,double> item: vmList)
        {
            double tm = computeMigrationTime(item.first,true,false);
            if(timeToMigrate + tm < timeslot && item.second > computeMigrationTime(item.first,true,true))
            {
                listToMigrate.push_back(item.first);
                timeToMigrate += tm;
            }
        }
        sortedVM[dc]=listToMigrate;        
    }

    return sortedVM;

}


/**
 * Compute the migration duration of a VM.
 * @param vm the VM.
 * @param interDC true if the migration is between servers of different DCs, false if its from servers on the same DC (consolidation) .
 * @param minLatency true if we want to compute the migration time in the best (i.e. faster) case, or not
 * @return the migration duration of the VM.
 */
double Manager::computeMigrationTime(DeployableVM *vm,const  bool& interDC,const  bool& minLatency)
{	
    
		double theLatency = intraDClatency;
		if(interDC && minLatency) 
        {
			theLatency = minInterDClatency;
		}else if(interDC) 
        {
			theLatency = maxInterDClatency;
		}
				
		double transferLatency = theLatency + gamma / bandwidth;
		
		transferLatency = round(transferLatency * 10000.) / 10000.;
		
		double migrationThroughputWithLatency = windowSize / (2. * transferLatency);
		
		double migrationThroughputWithBandwidth = bandwidthRatio * bandwidth;
		
		double migrationThroughput = min(migrationThroughputWithLatency, migrationThroughputWithBandwidth);
				
		double migrationTime =  3. * alpha * transferLatency + vm->getRamSize() * pow(1024,2) / migrationThroughput;
				
		return migrationTime;

}

/**
 * Plan running VM migrations from a given DC to another given one. 
 * This algorithm takes account of the preallocations previously made.
 * @param dcSending The ID of the DC sending the VMs.
 * @param dcReceiving The ID of the DC receiving the VMs.
 * @param listToMigrateSending The list of VMs to migrate. 
 * @param tmin The previous minimum time of the migrations.
 * @param tmax The previous maximum time of the migrations.
 * @return The previous values of tmin and tmax.
 */
double *Manager::planVmMigrationsForDCs(const std::string& dcSending,const  std::string& dcReceiving,std::vector<DeployableVM*>& listToMigrateSending,double& tmin, double& tmax,double *times)
{
    
    tmin = *computeTminTmaxTsend(tmin, tmax);
    tmax = *(computeTminTmaxTsend(tmin, tmax)+1);
    double tsend = *(computeTminTmaxTsend(tmin, tmax)+2);


    for(int i=0;i<listToMigrateSending.size();i++)
    {
        DeployableVM* vm = listToMigrateSending[i];
        double tm = computeMigrationTime(vm,true,false);// worse
        double tm_best = computeMigrationTime(vm,true,true);// best
        std::vector<std::string> hostsOn = getServersWithMinAvailableVolume(dcReceiving, false, vm->getNbCores());
        if(tsend + tm < tmax && hostsOn.size()!=0)
        {
            std::string targetHost;
            if(!serversInformation[hostsOn[0]]->serverIsOn())// isWorkerOffWithPreallocationsAndMigrations(hostsOn[0]))
            {
                for(std::string worker : hostsOn)
                {
                    if(serversInformation[worker]->serverIsOn())
                    {
                        targetHost=worker;
                        break;
                    }
                }
                continue;
            }
            else
            {
                 targetHost = hostsOn[0];
            }
            VMRequest * vmDesc = new VMRequest(vm->getNbCores(),vm->getRamSize(),vm->getRemainingDuration(),vm->getName());
            double Psending = powerConsumptions[dcSending];
            double Preceiving = powerConsumptions[dcReceiving];

            double tMax = round(simgrid::s4u::Engine::get_clock()) + vm->getRemainingDuration();

            bool condition = false;

            if(!optimizeFinancialCost)
            {
                double la =expectedBrownPowerUntilSunset(dcSending, Psending - costOfVm(vm), tMax);
                double lb =expectedBrownPowerUntilSunset(dcReceiving, Preceiving + vmAdditionCost(targetHost, vmDesc), tMax);
                double ra =expectedBrownPowerUntilSunset(dcSending, Psending, tMax); 
                double rb =expectedBrownPowerUntilSunset(dcReceiving, Preceiving, tMax);
                condition = la + lb < ra + rb;
            }
            else
            {
                double *P = new double[dcsName.size()];
                double *P_migration = new double[dcsName.size()];
                for(int j=0;j<dcsName.size();j++){
                    P[j] = powerConsumptions[dcsName[j]];
                    if(dcsName[j].compare(dcSending)==0)
                    {
                        *(P_migration + j) = *(P + j) - costOfVm(vm);
                    }
                    else if(dcsName[j].compare(dcReceiving) == 0)
                    {
                        *(P_migration + j) = *(P + j) + vmAdditionCost(targetHost, vmDesc);
                    }
                    else
                    {
                       *(P_migration + j) = *(P + j);
                    }
                }
                double migrationTime = computeMigrationTime(vm, true, false);
                double migration_Ecost = costOfVm(vm) * migrationTime + p_core * migrationTime;
                
                condition = expectedFinancialCostsUntilSunset(P_migration, tMax, migration_Ecost) < expectedFinancialCostsUntilSunset(P, tMax, 0);
            }

            if(condition && vm->getRemainingDuration() - tsend > tm)
            {
                powerConsumptions[dcSending] = powerConsumptions[dcSending] - costOfVm(vm);
                powerConsumptions[dcReceiving]= powerConsumptions[dcReceiving] + vmAdditionCost(targetHost,vmDesc);
                cout << round(simgrid::s4u::Engine::get_clock()) <<  ";" << vm->getName() << ";"<< vm->getWorkerActor()->getServerName() <<";"<< targetHost  << endl;
                /*
                System.out.println("Migration planned! "+"\nvm "+vm
                +"\nstart time  "+(Msg.getClock()+tsend)
                + "\ndc origin  "+(dcSending)
                +"\nserver origin  "+(vm.getLocalManager())
                +"\nserver origin DC name"+(vm.getLocalManager().getSiteName())
                +"\ndc dest "+(dcReceiving)
                +"\nserver dest  "+targetHost
                +"\nserver dest  DC name"+targetHost.getSiteName()
                +"\ntime to migrate (best) "+tm_best
                +"\ntime to migrate (worst) "+tm);					*/
                migrationsList.push_back(new MigrationPlan(round(simgrid::s4u::Engine::get_clock())+tsend,simgrid::s4u::Host::by_name(targetHost), vm, tm_best,tm));                
                tsend += tm;
                listToMigrateSending.erase(listToMigrateSending.begin()+ i);
                i--;
                prepareForNemesis();

            }


        }

    }

    tmax = tsend;    
    times[0] = tmin;
    times[1] = tmax;
    return times;

}

/**
 * Compute a time window where VM migrations should be performed for a DC.
 * @param tmax the upper bound of the previous window.
 * @return A tuple of the new values of (tmin,tmax,tsend), t_send corresponding to the time where the first migration should be done.
 
 * @param tmin the lower bound of the previous window. :*/
double *Manager::computeTminTmaxTsend(double tmin, double tmax)
{
    double *computedTimes = new double[3];    
    if(tmin==tmax)
    {
        tmin = 0;
        tmax = timeslot;
    }
    else
    {
        if(tmin <= timeslot - tmax)
        {
            tmin = tmax;
            tmax = timeslot;
        }
        else
        {
            tmax = tmin;
            tmin = 0;
        }
    }

    double tsend = tmin;   
    computedTimes[0] = tmin;
    computedTimes[1] = tmax;
    computedTimes[2] = tsend;
    return  computedTimes;

}

/**
 * Compute the power cost of a running VM.
 * If the VM is alone in it's host (considering the preallocations and pre-migrations previously made), then we consider that the cost include P_idle.
 * @param vm The running VM.
 * @return the energy cost of the VM.
 */
double Manager::costOfVm(DeployableVM *vm)
{    
    int nbCore = vm->getNbCores();
    std::string worker = vm->getWorkerActor()->getServerName();
    // the cost of the VM running
    double cost = nbCore * pcore;    
    bool isMigration = false;
    bool isInPreAlloc = false;
    for(MigrationPlan *migrationPlan : migrationsList)
    {
        if(migrationPlan->getHost()->get_name().compare(worker) == 0)
        {
            isMigration = true;
            break;
        }
    }    
    for(auto itemPreallocation = preallocations.begin();
            itemPreallocation!= preallocations.end();
            ++itemPreallocation )    
    {
        if(itemPreallocation->second.compare(worker)==0)
        {
            isInPreAlloc = true;
            break;
        }
    }
    if( ( simgrid::s4u::Host::by_name(worker)->get_core_count() -  
          serversInformation[worker]->getAvailableCores()) == nbCore &&
          !isInPreAlloc && !isMigration
      )
    {
        cost += p_idle;
    }
    return cost;
}

/**
 * Launch a migration manager that will starts the scheduled migration.
 */
void Manager::startMigrations()
{

 
    for( MigrationPlan* migration : migrationsList )
    {
        WorkerInformation* wi = serversInformation[migration->getHost()->get_name()];        
        MailBoxSingleton::GetInstance()->sendMessage("worker_"+migration->getHost()->get_name(), new Message(MESSAGE_START_VM_MIGRATION,migration)); 
    }
}

void Manager::prepareForNemesis()
{            
    dcStdDevMap.clear();
    dcEnergyStdDevMap.clear();
    for(std::string dc : dcsName)
    {                          
        std::vector<std::string>::iterator it = dcServers[dc].begin();
        std::vector<std::string>::iterator end = dcServers[dc].end(); 
        dcWorkersInfo[dc].clear();   
        dcHostsExtinctionTimes[dc].clear();
        dcVMExtinctionTimes[dc].clear();
        while(it != end)
        {
            if(serversInformation.find(*it) != serversInformation.end() )
            {
                dcWorkersInfo[dc].push_back( serversInformation[*it] );                       
            }
            ++it;
        }
        sort(dcServers[dc].begin(), dcServers[dc].end());     
    }
    dcHostsExtinctionTimes.clear();
    dcVMExtinctionTimes.clear();
}


void Manager::consolidateDC(std::string site)
{
    //validate if it will copy automatically...
    //we save the preallocations and migrations previously scheduled
    robin_hood::unordered_map<VMRequest*, std::string> preallocationsSave = preallocations;
    std::vector<MigrationPlan*> migrationsListSave = migrationsList;

    int nbChange = 0;

    // first, we build the list of incomplete servers ON sorted by decreasing available volume on the DC.
    std::vector<std::string> servers = getSortedServers(false);    
    
    std::vector<std::string> dcServers;

    for(std::string server : servers)   
    {
        WorkerInformation *workerInfo = serversInformation[server];
        int availableCores = nbAvailableCoresWithPreallocationsAndMigrations(workerInfo);
        if(
            (*workerInfo->getDCName()).compare(site) == 0 //same DC 
            && availableCores > 0 //not full yet
            && availableCores < simgrid::s4u::Host::by_name((*workerInfo->getHostName()))->get_core_count()  // is not empty, at least one core is being used 
            && serversInformation[server]->serverIsOn() // !isWorkerOffWithPreallocationsAndMigrations(server) // the server is not off

        )
        {
            dcServers.push_back(server);
        }        
    }


    int k = dcServers.size() /2;
    int i = (int) ceil(dcServers.size()/2.0);
    bool sol = true;

    while(i > 1)
    {

        preallocations = preallocationsSave;
        migrationsList = migrationsListSave;
        nbChange = 0;
        // build the list of non-migrating VMs deployed or preallocated on the k first servers
        std::vector<VMInterface*> vms = buildNonMigratingVMSortedList(dcServers, k);
        std::vector<VMInterface*>::iterator vmIterator = vms.begin();
        double migtime= 0.0;

        while(vmIterator!= vms.end() != 0 && sol)
        {
            VMInterface* vm = *vmIterator;
            // we search the last possible server which can receive the first VM
            WorkerInformation *server = searchLastPossibleServerForVM(vm,dcServers,k);
            if(server!=nullptr)
            {
                // then if the vm is already deployed, we schedule (or change the destination if the vm is planned to migrate from another DC) 
                // at the opposite if the vm is only preallocated, we simply change the preallocation.
                if( dynamic_cast<DeployableVM*>(vm) != nullptr )
                {
                    bool isAlreadyMigrating = dynamic_cast<DeployableVM*>(vm)->isVMMigrating();
                    if(!isAlreadyMigrating)
                    {
                        for(MigrationPlan *mig : migrationsList)
                        {
                            if(mig->getVM()== static_cast<DeployableVM*>(vm) )
                            {
                                isAlreadyMigrating = true;
                                mig->setHost(simgrid::s4u::Host::by_name(*server->getHostName()));
                                nbChange++;
                                break;
                            }
                        }
                    }
                    if(!isAlreadyMigrating)
                    {                    
                        double tm_best= computeMigrationTime(static_cast<DeployableVM*>(vm),false,true);
                        double tm_worse= computeMigrationTime(static_cast<DeployableVM*>(vm),false,false);;
                        migrationsList.push_back(new MigrationPlan(round(simgrid::s4u::Engine::get_clock()+ migtime),simgrid::s4u::Host::by_name(*server->getHostName()), static_cast<DeployableVM*>(vm), tm_best,tm_worse));                                        
                        migtime += tm_worse;
                        //System.out.println("Migration planned for consolidation! ");                        
                        //System.out.println("vm "+vm);
                        //System.out.println("start time  "+(Msg.getClock()));
                        //System.out.println("dc origin  "+(site));
                        //System.out.println("server origin  "+(((DeployableVM)vm).getLocalManager()));                        
                        //System.out.println("server dest  "+server);
                        //System.out.println("time to migrate (best) "+tm_best);
                        //System.out.println("time to migrate (worst) "+tm_worse);                                                
                        nbChange++;						                        
                        cout << round(simgrid::s4u::Engine::get_clock()) <<  ";" << vm->getName() << ";"<< static_cast<DeployableVM*>(vm)->getWorkerActor()->getServerName() <<";"<< *server->getHostName()  << endl;
                    }

                }
                else if(dynamic_cast<VMRequest*>(vm) != nullptr)
                {
                        preallocations[dynamic_cast<VMRequest*>(vm) ]=*server->getHostName();						
						nbChange++;
                }
                ++vmIterator;
            }
            else 
            {
                sol = false;
            }
        }

        i = (int) ceil(i / 2.0);	
        if(sol)
        {
            k+=i;
        }
        else
        {
            k-=i;
        }
    }
    if(!sol)
    {
        // we remove the previous modifications of the consolidation algorithm
        preallocations = preallocationsSave;
        migrationsList = migrationsListSave;
        nbChange = 0;
        k--;
        // build the list of non-migrating VMs deployed or preallocated on the k first servers
        std::vector<VMInterface*> vms = buildNonMigratingVMSortedList(dcServers, k);
        std::vector<VMInterface*>::iterator vmIterator = vms.begin();
        double migtime = 0.0;
        while(vmIterator!= vms.end() != 0 )
        {
            
            VMInterface* vm = *vmIterator;
            // we search the last possible server which can receive the first VM
            WorkerInformation *server = searchLastPossibleServerForVM(vm,dcServers,k);
            if(server!=nullptr)
            {
                // then if the vm is already deployed, we schedule (or change the destination if the vm is planned to migrate from another DC) 
                // at the opposite if the vm is only preallocated, we simply change the preallocation.
                if( dynamic_cast<DeployableVM*>(vm) != nullptr )
                {
                    bool isAlreadyMigrating = dynamic_cast<DeployableVM*>(vm)->isVMMigrating();
                    if(!isAlreadyMigrating)
                    {
                        for(MigrationPlan *mig : migrationsList)
                        {
                            if(mig->getVM()== static_cast<DeployableVM*>(vm) )
                            {
                                isAlreadyMigrating = true;
                                mig->setHost(simgrid::s4u::Host::by_name(*server->getHostName()));
                                nbChange++;
                                break;
                            }
                        }
                    }
                    if(!isAlreadyMigrating)
                    {
                    
                        double tm_best= computeMigrationTime(static_cast<DeployableVM*>(vm),false,true);
                        double tm_worse= computeMigrationTime(static_cast<DeployableVM*>(vm),false,false);;
                        migrationsList.push_back(new MigrationPlan(round(simgrid::s4u::Engine::get_clock()+ migtime),simgrid::s4u::Host::by_name(*server->getHostName()), static_cast<DeployableVM*>(vm), tm_best,tm_worse));                                        
                        migtime += tm_worse;
                        cout << round(simgrid::s4u::Engine::get_clock()) <<  ";" << vm->getName() << ";"<< static_cast<DeployableVM*>(vm)->getWorkerActor()->getServerName() <<";"<< *server->getHostName()  << endl;
                        //System.out.println("Migration planned for consolidation! ");                        
                        //System.out.println("vm "+vm);
                        //System.out.println("start time  "+(Msg.getClock()));
                        //System.out.println("dc origin  "+(site));
                        //System.out.println("server origin  "+(((DeployableVM)vm).getLocalManager()));                        
                        //System.out.println("server dest  "+server);
                        //System.out.println("time to migrate (best) "+tm_best);
                        //System.out.println("time to migrate (worst) "+tm_worse);                                                
                        nbChange++;						                        
                    }

                }
                else if(dynamic_cast<VMRequest*>(vm) != nullptr)
                {
                        preallocations[dynamic_cast<VMRequest*>(vm) ]=*server->getHostName();						
						nbChange++;
                }
                ++vmIterator;
            }
            else 
            {
                //THROW ERROR IMPOSSIBLE TO FIND PRE ALLOC???   sol = false;
            }
        }        

    }



}



	
/**
 * Return the list of non-migrating and non imminently terminating (in the sense that the VM will not terminate during migration) VMs deployed (or preallocated) on the k first server of a given list.
 * The VMs are sorted by decreasing volume.
 * We consider that a VM is non-migrating either if it is actually migrating or if we've simply planned to migrate it.
 * @param dcServers The list of servers.
 * @param k The number of server we want to consider in the list.
 * @return The list of non-migrating VM.
 */
std::vector<VMInterface*> Manager::buildNonMigratingVMSortedList(const std::vector<std::string>& dcServers, int& k)
{
    vector<VMInterface*> vms;
    // TODO: if the dcServers contains immovable VMs, then the consolidation algorithm may set k greater than the number of servers.
    // it may not be the cleanest way of doing this.
    k = min(k, (int) dcServers.size());

    for(int i=0; i <k ; i++ )
    {
        WorkerInformation *worker = serversInformation[dcServers.at(i)];
        robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = worker->getRunningVMS();

        //first, we consider the running VMs.
        if( runningVMSMap!=nullptr && !(*runningVMSMap).empty() )   
        {

            for(auto item = runningVMSMap->begin(); item != runningVMSMap->end(); ++item)
            {
                // we check if we already plan the migrate this VM.
                bool planToMigrate = false;
                for(MigrationPlan *mig : migrationsList)
                {
                    if(mig->getVM() == item->second)
                    {
                        planToMigrate = true;
                        break;
                    }
                }

                if
                (
                    !item->second->isVMMigrating() &&
                    !planToMigrate &&
                    item->second->getRemainingDuration() > computeMigrationTime(item->second,false,true)
                )
                {
                    vms.push_back(item->second);
                }
            }
        }
        //then, the preallocated VMs.
        for(auto itemPreallocation = preallocations.begin();
                itemPreallocation!= preallocations.end();
                ++itemPreallocation )    
        {
            if(itemPreallocation->second.compare(*worker->getHostName()) == 0)
            {
                vms.push_back(itemPreallocation->first);
            }
        }
        
        // finally, the incoming migrating VMs (because we can change the destination of these migration
        //TODO: MIGUEL can add in the above loop
        for(MigrationPlan *mig : migrationsList)
        {
            if(mig->getHost()->get_name().compare(*worker->getHostName()) == 0)
            {
                vms.push_back(mig->getVM());
            }
        }
    }
    //TODO: MIGUEL VALIDATE IF IT IS WORKINGGG
    std::stable_sort(vms.begin(), vms.end(), sortVMByCPU);           
    return vms;
}


/**
 * Search in a given list the last possible server which can receive a given VM.
 * The method does not take account of the k first servers.
 * @param vm The VM.
 * @param dcServers The list of servers.
 * @param k The limit of servers which are not taken into account.
 * @return The last possible server which can receive the VM.
 */
WorkerInformation *Manager::searchLastPossibleServerForVM(VMInterface *vm, std::vector<std::string> dcServers, int k)
{
    WorkerInformation *server = nullptr;
    std::vector<std::string>::iterator dcServersIterator = --dcServers.end();
    for(int index = dcServers.size() -1; index >= k; index--  )
    {
        if(nbAvailableCoresWithPreallocationsAndMigrations(*dcServersIterator) >= vm->getNbCores() )
        {
            server = serversInformation[*dcServersIterator];
            break;
        }
        --dcServersIterator;
    }

    return server;
}

void Manager::shutDownEmptyHosts()
{
 
    robin_hood::unordered_set<std::string> hostsInMigrations;
    robin_hood::unordered_set<std::string> hostsInPreallocations;    
    
    for(auto itemPreallocation = preallocations.begin();
            itemPreallocation!= preallocations.end();
            ++itemPreallocation )    
    {
        hostsInPreallocations.insert(itemPreallocation->second);
    }   

    for(MigrationPlan* mig : migrationsList)
    {                
        hostsInMigrations.insert(mig->getHost()->get_name());
    }        

    for(auto it =serversInformation.begin() ; it !=serversInformation.end(); ++it )
    {

        robin_hood::unordered_map<std::string, DeployableVM*>  *runningVMSMap = it->second->getRunningVMS();
        if
        ( 
            ( runningVMSMap==nullptr || (*runningVMSMap).empty() ||(*runningVMSMap).size()==0 ) 
            && hostsInMigrations.find(it->first) == hostsInMigrations.end()
            && hostsInPreallocations.find(it->first) == hostsInPreallocations.end() 
            && it->second->serverIsOn()
        )                        
        {
      //      cout << "off - "  << it->first << endl;
            MailBoxSingleton::GetInstance()->sendMessage(*it->second->getDCName(),new Message(MESSAGE_SHUTDOWN_HOST, &it->first) );  
        }
    } 
    

}


double Manager::expectedFinancialCostsUntilSunset(double *P,const double& t_max,const double& EconsumptionInit)
{			
    double time = round(simgrid::s4u::Engine::get_clock());            
    double *standardDev = new double[dcsName.size()];
    double globalStandardDev = 0;
    for(int i = 0;i<dcsName.size();i++)
    { 
        *(standardDev +i) = (sellBuyPeriod/timeslot) * pow(energyStandardDeviation(dcsName[i]),2);
        globalStandardDev +=  *(standardDev +i);
        *(standardDev +i) = sqrt( *(standardDev +i));
    }
    
    globalStandardDev = sqrt( globalStandardDev);
        
    // we start at the beginning of the current longest trading period (i.e. autoconso or sell/buy) ;
    double min_bound_sellBuy = time - std::fmod(time , sellBuyPeriod);
    
    //double min_bound = Math.min(min_bound_autoconso, min_bound_sellBuy);	
    double max_bound = computeCostComputationMaxBound(time, t_max);
        
    double costs = computeGlobalCostsUntilSunrise(min_bound_sellBuy, max_bound, P, globalStandardDev,sellBuyPeriod,selling_gain,buying_cost, EconsumptionInit, (1.0/(3600.0 * 1000.0)), standardDev);
    //double autoConsoCosts = computeGlobalCostsUntilSunrise(min_bound_autoconso,max_bound,P,globalStdDev_autoConso,autoconsoPeriod,autoconso_gain,autoconso_cost, EconsumptionInit, (1.0/(3600.0 * 1000.0 * 1000.0)));
    //double utilisationCosts = computeUtilisationCostsUntilSunrise(Msg.getClock(),max_bound, P, standardDev);
    
    return costs;

}

/**
 * Compute the standard deviation of the green power production of a given DC for the current period.
 * @param dc The ID of the DC.
 * @return The standard deviation.
*/
double Manager::energyStandardDeviation (const std::string& dc)
{		
    std::map<double,double>& greenPowHistory = greenPower[dc];		
    double stdDev = 0;		
    if(greenPowHistory.size()!=0) 
    {							        
        double average = avg(dc) * timeslot;
        double sumGreenPow = 0;

        // we compute the sum of the squared deviations of the values in the history (i.e. the already received events) from the mean
        std::map<double,double>::iterator end =  greenPowHistory.end();        
        
        for(std::map<double,double>::iterator it = greenPowHistory.begin(); it != end; ++it)
        {            
            sumGreenPow += pow( (it->second * timeslot) -average , 2);				            
        }            
        double variance = sumGreenPow / greenPowHistory.size();			
        stdDev = sqrt(variance);		
    }				
    dcEnergyStdDevMap[dc] = stdDev;
    return stdDev;
}


/**
 * Compute the maximum time to consider when computing cost for a current time.
 * This maximum time is computed in order to stop at sunset and after completed trading periods (autoconso or sell/buy).
 * @param time the current time.
 * @param t_max The maximum bound to consider.
 * @return the maximum time to consider when computing cost.
 */
double Manager::computeCostComputationMaxBound(const double& time,const double& t_max)
{
    // we don't want to stop in the middle of a trading period (i.e. autoconso or sell/buy)
    double max_bound = t_max;
    double gapWithPeriod = std::fmod(t_max , sellBuyPeriod);

    if(gapWithPeriod > 0)
    {
        max_bound += sellBuyPeriod - gapWithPeriod;
    }
    
    double n_eval_bound = time + timeslot * n_eval;
    gapWithPeriod = (double) std::fmod( n_eval_bound,sellBuyPeriod);
    
    if(gapWithPeriod > 0)
    {
        n_eval_bound += sellBuyPeriod - gapWithPeriod;
    }
    
    // we also want to stop at the next sunset.
    double sunsetTime = time;
    double idealPow = getIdealPowerAtTime(time, dcsName[0]);
    // if we start at night, then we search the next sunset
    bool isNight = (idealPow == 0);
    while(idealPow > 0 || isNight)
    {
        sunsetTime += sellBuyPeriod;
        idealPow = getIdealPowerAtTime(sunsetTime,dcsName[0]);
        if(isNight && idealPow > 0)
        {
            isNight = false;
        }
    }
    return min(n_eval_bound, min(max_bound, sunsetTime));
}
	
double Manager::computeGlobalCostsUntilSunrise(double t_min,const double& t_max, double *P,const double& standardDev,const double& aPeriod,const double& aGain,const double& aCost,const double& totalE_init,const double& scale,double* standardDev_dc)
{
		
    double cost = 0;
    
    double *total_P = new double[dcsName.size()];
    for(int i=0;i<dcsName.size();i++){
        *(total_P+i) = *(P+i);
    }
    
    std::vector<map<double,std::vector<VMInterface*>>> vmExtinctionTimes;
    std::vector<map<double,int>> hostExtinctionTimes;
    
    
    for(std::string dc :dcsName)
    {

        if(dcVMExtinctionTimes.find(dc)== dcVMExtinctionTimes.end() )
        {    
            getVmExtinctionTimes(dc);
        }    
        map<double,std::vector<VMInterface*>>& dcVmExtinctionTimes = dcVMExtinctionTimes[dc];
        vmExtinctionTimes.push_back(dcVmExtinctionTimes);
        if(dcHostsExtinctionTimes.find(dc)== dcHostsExtinctionTimes.end())
        {
            getHostsExtinctionTimes(dc, dcVmExtinctionTimes);
        }      
        map<double,int>& dcHostExtinctionTimes= dcHostsExtinctionTimes[dc];
        hostExtinctionTimes.push_back(dcHostExtinctionTimes);
    }
    
    bool firstStep = true;
    
    // for each period of trading between t_min and t_max
    while(t_min < t_max){
        
        // first, we compute, the sum of the energy consumption of the DC for the period.
        double total_E = 0;
        double *E_dc = new double[dcsName.size()];
        
        if(firstStep)
        {
            total_E = totalE_init;
            firstStep = false;
        }
        
        for(int i = 0;i<dcsName.size();i++)
        {
            
            double currentTime = t_min;
            *(E_dc + i) =0;
            // if some timeslots are already passed, then, we used the recorded values.
            if(t_min < round(simgrid::s4u::Engine::get_clock()))
            {						

                double hist_a =consumptions_historic[dcsName[i]][round(simgrid::s4u::Engine::get_clock())];
                double hist_b =  consumptions_historic[dcsName[i]][t_min];
                *(E_dc + i) += hist_a -hist_b ;
                currentTime =round(simgrid::s4u::Engine::get_clock());
            }
            
            map<double,std::vector<VMInterface*>>& vm_events = vmExtinctionTimes[i];
            map<double,int>& host_events = hostExtinctionTimes[i];
            
            map<double,std::vector<VMInterface*>>::iterator vm_events_it = vm_events.begin();
            map<double,int>::iterator host_events_it = host_events.begin();

            while(( vm_events_it !=  vm_events.end() && (*vm_events_it).first <  (t_min+aPeriod) )
            
            ||( host_events_it != host_events.end()  && (*host_events_it).first < (t_min+aPeriod)))
            {                                
                double vm_eventTime = (vm_events_it !=  vm_events.end())? (*vm_events_it).first : std::numeric_limits<double>::max() ;
                double host_eventTime = (host_events_it != host_events.end())? (*host_events_it).first : std::numeric_limits<double>::max() ;			
                double eventTime = min(vm_eventTime, host_eventTime);
                
                *(E_dc + i) += *(total_P+ i) * (eventTime - currentTime);
                currentTime = eventTime;
                
                if(vm_eventTime == eventTime){
                    for(VMInterface* vm : vm_events_it->second)
                    {
                        *(total_P +i) -=  vm->getNbCores() * p_core;                        
                    }     
                    vm_events.erase(vm_events_it++);               
                    
                }
                
                if(host_eventTime == eventTime){
                    int nbHostOff = (*host_events_it).second;
                    *(total_P + i) += nbHostOff * (p_down - p_idle);
                    *(E_dc + i) += nbHostOff * p_off * hostPoweringOffDelay;
                    host_events.erase(host_events_it++);
                    
                }
            }
            
            *(E_dc + i) += *(total_P + i) * ( t_min + aPeriod - currentTime);
            total_E += *(E_dc + i);
            
        }
                    
        // then we compute the sum of the energy production of the DC for the period.
        double total_green = 0;
        double *green_DC = new double[dcsName.size()];
        double currentTime = t_min;
        // if some timeslots are already passed, then we substract the green energy of the consumption
        // we do this because those green production are known and thus are not stochastic variable like total_green
        if(t_min < round(simgrid::s4u::Engine::get_clock()))
        {
            while( currentTime < round(simgrid::s4u::Engine::get_clock()) )
            {	
                for(int i= 0;i<dcsName.size();i++)
                {
                    std::string dc = dcsName[i];
                    if(greenPower[dc].size()>0 && greenPower[dc].find(currentTime) !=greenPower[dc].end())
                    {
                        double minus = greenPower[dc][currentTime] * timeslot;
                        total_E -= minus;
                        *(E_dc+ i) -= minus ;
                    }else{  
                        ////system.out.printl("WARNING, no entry in green pow historic (size ="+greenPower.get(dc).size()+ ") for time "+currentTime);
                    }
                }
                currentTime += timeslot;
            }
            currentTime = round(simgrid::s4u::Engine::get_clock());
        }
        
        while(currentTime < t_min+aPeriod)
        {	
            for(int i=0;i<dcsName.size();i++){
                std::string dc = dcsName[i];
                *(green_DC+i) = expectedGreenPow(dc,currentTime) * timeslot;
                total_green +=*(green_DC + i);
            }
            currentTime += timeslot;
        }
        
        // finally we estimate the cost of this trading period.
        cost += computeGlobalCost(total_E, total_green, standardDev, aCost, aGain, scale);

        double total_prod = 0;
        double total_conso = 0;

        for(int i=0;i < dcsName.size();i++)
        {
            total_prod += expectedRemainingGreenPow(dcsName[i], *(E_dc+i), *(standardDev_dc+i), t_min);
            total_conso += expectedBrownEnergy(dcsName[i], *(E_dc+i), *(green_DC + i), t_min, *(standardDev_dc+i));
        }
        
        double utilisationCost = fullUtilisationCost;
        if(std::fmod( t_min , (fullCostPeriod + lightCostPeriod)) >= fullCostPeriod){
            utilisationCost = lightUtilisationCost;
        }
        
        cost += min(total_prod, total_conso) * utilisationCost / (3600.0 * 1000.0);        
        t_min += aPeriod;
    }	    
    return cost;
}


double Manager::computeGlobalCost(const double& consumption,const double& production,const double& standardDev,const double& cost,const double& gain,const double& scale)
{

    double ret = cost * scale * (consumption - production);

    if(standardDev != 0)
    {
        double the_cost = consumption - production;
        the_cost *= (Phi(consumption,production,standardDev) - Phi(0,production,standardDev))/(1.0-Phi(0,production,standardDev));
        the_cost -= pow(standardDev,2) * (phi(0,production,standardDev)-phi(consumption,production,standardDev)) / (1.0 - Phi(0,production,standardDev));
        the_cost *= cost * scale;
        
        double the_gain = production - consumption;
        the_gain *= (1 - Phi(consumption,production,standardDev))/(1-Phi(0,production,standardDev));
        the_gain += pow(standardDev, 2) * phi(consumption,production,standardDev) / (1-Phi(0,production,standardDev)); 
        the_gain *= gain * scale;
        
        ret = the_cost - the_gain;
        
    }else if(consumption - production < 0){
        ret = - gain * scale * (production - consumption);
    }

    if(isnan(ret))
    {
        cout << "expected brown power for cloud = " <<ret <<" greenPow = "<<production<<" standardDev = "<<standardDev<<" Phi(P) = "<<Phi(consumption,production,standardDev)<<" Phi(0,) = "<<Phi(0,production,standardDev)<<" phi(P) = "<<phi(consumption,production,standardDev)<<" phi(0,) = "<<phi(0,production,standardDev) << endl;
    } 
    return ret;
}


double Manager::expectedBrownEnergy(const std::string& dc,const double& P,const double& greenPow,const double& t,const double& standardDev) 
{
    double ret = P - greenPow;
    if(standardDev != 0)
    {
        ret *= (Phi(P,greenPow,standardDev) - Phi(0,greenPow,standardDev))/(1.0-Phi(0,greenPow,standardDev));
        ret -= pow(standardDev,2) * (phi(0,greenPow,standardDev)-phi(P,greenPow,standardDev)) / (1.0 - Phi(0,greenPow,standardDev));
    }
    
    if(isnan(ret))
    {
        cout << "expected brown power for "<<dc <<" = "<<ret<<" greenPow = "<<greenPow<<" standardDev = "<<standardDev<<" Phi(P) = "<<Phi(P,greenPow,standardDev)<<" Phi(0,) = "<<Phi(0,greenPow,standardDev)<<" phi(P) = "<<phi(P,greenPow,standardDev)<<" phi(0,) = "<<phi(0,greenPow,standardDev) << endl;
    }
     
    return ret;
}