#ifndef UTILISATIONCOSTSWITCHACTOR_HPP
#define UTILISATIONCOSTSWITCHACTOR_HPP

#include <simgrid/s4u.hpp>
#include "MailBoxSingleton.hpp"
#include "FinancialSystem.hpp"
#include "CommunicationInterface.hpp"
#include <math.h>  

class UtilisationCostSwitchActor: CommunicationInterface 
{


private:
FinancialSystem *financial =nullptr;
double fullCost;
double lightCost;
double fullCostPeriod;
double lightCostPeriod;
	
public:
explicit UtilisationCostSwitchActor();
explicit UtilisationCostSwitchActor(const double& aFullCost, const double& aLightCost, const double& aFullCostPeriod,const double& aLightCostPeriod,FinancialSystem* aFinancial);
void operator()();
void handleMessage(Message *message);


};


#endif

