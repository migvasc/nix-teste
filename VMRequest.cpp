#include "VMRequest.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(vmrequest, "vmrequest category");

VMRequest::VMRequest(int cores, double ram, double time, std::string vmID, const std::vector<std::pair<int,int>>& history){
    
    resourceUsageHistory = history;
    ncores = cores;
    ramsize = ram;    
    id = vmID;
    duration = time;
    flopAmount = duration * 11770000000.0;
    
}

VMRequest::VMRequest(int cores, double ram, double time, std::string vmID){
    //XBT_INFO("CONSTRUTOR DA VM: cores %d, ram %f, ID %s ",cores,ram,vmID.c_str());
    ncores = cores;
    ramsize = ram;    
    id = vmID;
    duration = time;
    flopAmount = duration * 11770000000.0;
}

double VMRequest::getRamSize(){
    return ramsize;
}
int VMRequest::getNbCores(){
    return ncores;
}

double VMRequest::getFlopAmount(){    
    return flopAmount;
}

std::string VMRequest::getName(){
    return id;
}

simgrid::s4u::VirtualMachine* VMRequest::getVM(){
    return vm;
}

void VMRequest::setVM(simgrid::s4u::VirtualMachine* vm_){
    vm = vm_;
}


void VMRequest::setDestHost(std::string host){
    destHost = host;
}

std::string VMRequest::getDestHost(){
    return destHost;
}

double VMRequest::getRemainingDuration()
{   
    return duration;
}


std::vector<std::pair<int,int>>  VMRequest:: getVMResourceUsageHistory()
{
    return resourceUsageHistory;   
}