#ifndef ELECTRICGRID_HPP
#define ELECTRICGRID_HPP

#include <simgrid/s4u.hpp>
#include "ElectricSystem.hpp"
#include "Message.hpp"
#include "MailBoxSingleton.hpp"
#include "EnergyInformation.hpp"
#include "CommunicationInterface.hpp"
#include "ElectricDC.hpp"
#include "FinancialSystem.hpp"
#include <mutex> /* std::mutex and std::lock_guard */
#include <math.h>  

/**
 * The electric system of the cloud.
 * This system is composed of several subsystems : the electric systems of the data-centers composing the cloud.
 * 
 */
class ElectricGrid : ElectricSystem,CommunicationInterface {

private:
    simgrid::s4u::CommPtr comm_received = nullptr;   	
    std::vector<std::string> dcs;
    virtual void updateEnergy(double newTime);
    double lastTime = -1;
	std::vector<std::string> argsClass;
	void handleMessage(Message *message);
	void handleGetGridEnergyInfo();
	simgrid::s4u::MutexPtr mutex;
	/*SCORPIOUS Parameters*/
		/*
	 * FINANCIAL MODEL PARAMETERS.
	 */
	
	/**
	 * Cost of buying energy (in euros/kWh)
	 */
	double aBuying_cost = 0.15 ;
	
	/**
	 * Gain of selling energy (in euros/kWh).
	 */
	double aSelling_gain = 0.06;
	
	/**
	 * Cost of negative auto-consomation gap (in euros/MWh).
	 */
	double anAutoconso_cost = 63.63;
	
	/**
	 * Gain of positive auto-consomation gap (in euros/MWh).
	 */
	double anAutoconso_gain = 54.21;
	
	/**
	 * Price of power network utilisation at peak hours (in euros/kWh).
	 */
	double aFullUtilisationCost = 0.0418;
	
	/**
	 * Price of power network utilisation at off-peak hours (in euros/kWh).
	 */
	double aLightUtilisationCost = 0.0281;
	
	/**
	 * Duration of peak time (in sec).
	 */
	double aFullCostPeriod = 16 * 3600;
	
	/**
	 * Duration of off-peak time (in sec).
	 */
	double aLightCostPeriod = 8 * 3600;
	
	/**
	 * Period of auto-conso paying (in sec).
	 */
	double autoconsoPeriod = 30 * 60;
	
	/**
	 * Period of energy trading (in sec). 
	 */
	double sellBuyPeriod = 10 * 60;
	
	FinancialSystem *financial;	
public:

	/**
	 * Creates the electric system of a single data-center.
	 */
	explicit ElectricGrid(std::vector<std::string> args);

    /**
	 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
	 * (called by the master at the end of the simulation).
	 */		
    void operator()();


};

#endif