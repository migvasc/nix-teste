#include "DeployableVM.hpp"
#include "WorkerActor.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(dvm, "DeployableVM category");

DeployableVM::DeployableVM(std::string id, simgrid::s4u::Host* host, int ncores, double ram, double flops, WorkerActor *actor){    
    computeAmount = flops;
    workerActor = actor;
    isMigrating = false;
    vm = new simgrid::s4u::VirtualMachine(id,host,ncores, 1L* ncores * ram);//2GB per core
}

simgrid::s4u::VirtualMachine* DeployableVM::getVM(){
    return vm;
}

double DeployableVM::getRuntime(){
    double now = round(simgrid::s4u::Engine::get_clock()) ;
    return now - startTime;
}

double DeployableVM::getRemainingDuration(){

    double duration = 0;
    
    //Get the remaining amount of flops
    for(simgrid::s4u::ExecPtr exec : executions)    
        duration+=exec->get_remaining();
    
    //Calculate the average amount of flops per core
    duration = duration/vm->get_core_count();
    //Calculate the remaining time Flops to be computed divided by the host speed (FLOP/s)
    duration = duration/vm->get_pm()->get_speed();    
    return duration;

}

void DeployableVM::startVM(){    
    vm->start();    
    startsExecuting();
}

void DeployableVM::startsExecuting()
{  
    simgrid::s4u::Actor::create("exc_"+vm->get_name(), vm, VMExecutor(this, computeAmount,&executions));        
}

bool DeployableVM::isVMMigrating(){
    return isMigrating;
}

void DeployableVM::setIsVMMigrating(bool migrating){
    isMigrating = migrating;
}


void DeployableVM::setWorkerActor(WorkerActor *actor){
    workerActor =actor;
}


WorkerActor *DeployableVM::getWorkerActor()
{
    return workerActor;
}


std::string DeployableVM::getName(){
    return vm->get_name();
}
int DeployableVM::getNbCores(){
    return vm->get_core_count();
}


double DeployableVM::getRamSize(){
    return  getNbCores() * 2048.0;
}

double DeployableVM::getcurrentCPUUsage()
{
    return currentCPUUsage;
}

void DeployableVM::setcurrentCPUUsage(double usage)
{
    currentCPUUsage = usage;
}

double DeployableVM::getCurrentRAMUsage()
{
    return currentRAMUsage;
}

void DeployableVM::setCurrentRAMUsage(double usage)
{
    currentRAMUsage = usage;
}
